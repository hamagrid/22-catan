#include <iostream>

#include "../include/AssetManager.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	AssetManager::AssetManager()
	{
	}

	AssetManager::~AssetManager()
	{
		for (const auto&[id, texture] : m_textures)
		{
			delete texture;
		}
		for (const auto&[id, font] : m_fonts)
		{
			delete font;
		}
		//Krecemo od prvog igraca
		for (int i = 1; i < m_players.size(); i++)
		{
			delete m_players[i];
		}
	}

	/**************** Getters ****************/

	sf::Texture* AssetManager::get_texture(Texture_ID id) const
	{
		return m_textures.at(id);
	}

	const sf::Font& AssetManager::get_font(int id) const
	{
		return *(m_fonts.at(id));
	}

	std::vector<Player*>& AssetManager::get_players()
	{
		return m_players;
	}

	/**************** Other Methods ****************/

	void AssetManager::add_texture(Texture_ID id, const std::string& filePath)
	{
		auto texture = new sf::Texture();
		texture->setSmooth(true);

		if (texture->loadFromFile(filePath))
		{
			auto is_id_free_to_use = m_textures.find(id);
			if (is_id_free_to_use != m_textures.end())
			{
				delete m_textures[id];
			}
			m_textures[id] = texture;
		}
		else
		{
			std::cout << __FILE__ << " " << "Failed load texture" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}

	void AssetManager::add_font(int id, const std::string& filePath)
	{
		auto font = new sf::Font();

		if (font->loadFromFile(filePath))
		{
			auto is_id_free_to_use = m_fonts.find(id);
			if (is_id_free_to_use != m_fonts.end())
			{
				delete m_fonts[id];
			}
			m_fonts[id] = font;
		}
		else
		{
			std::cout << __FILE__ << " " << "Failed load font" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	void AssetManager::create_players(std::vector<catan_game::Player*>& players)
	{
		m_players = std::move(players);
	}

}// namespace catan_game