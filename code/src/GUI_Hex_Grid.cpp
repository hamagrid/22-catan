#include "../include/GUI_Hex_Grid.hpp"
#include "../include/GUI_Line_Object.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_Hex_Grid::GUI_Hex_Grid(
		GameState* game_state, Context* context, float first_hex_x, float first_hex_y, float first_hex_side_length
	)
		: m_game_state(game_state), m_context(context), m_first_hex_side_length(first_hex_side_length)
	{
		const auto& hexagons = m_game_state->get_board()->get_hexagons();
		m_gui_hexagons.resize(hexagons.size());

		for (const auto& hexagon : hexagons)
		{
			m_gui_hexagons[hexagon->get_id()] = new GUI_Hex(
				first_hex_x
				+ static_cast<float>(hexagon->get_column()) * (2 * (std::sqrt(3.f) * first_hex_side_length / 2))
				+ row_offset_fix_x(hexagon->get_row())
				, first_hex_y + static_cast<float>(hexagon->get_row()) * (2 * first_hex_side_length)
				  + column_offset_fix_y(hexagon->get_row())
				, hexagon->get_type()
				, hexagon->get_number()
				, first_hex_side_length
				, m_context
				, hexagon
			);
		}
		set_nodes_coordinates();
		set_roads_coordinates();

		m_gui_houses[Owners::Player1] = new GUI_House(0, 0, Owners::Player1, m_context);
		m_gui_houses[Owners::Player2] = new GUI_House(0, 0, Owners::Player2, m_context);
		m_gui_houses[Owners::Player3] = new GUI_House(0, 0, Owners::Player3, m_context);
		m_gui_houses[Owners::Player4] = new GUI_House(0, 0, Owners::Player4, m_context);

		for (const auto&[house_owner, house] : m_gui_houses)
		{
			const float house_slacing_factor = .17f;
			house->set_scale(house_slacing_factor, house_slacing_factor);
		}

		m_gui_cities[Owners::Player1] = new GUI_City(0, 0, Owners::Player1, m_context);
		m_gui_cities[Owners::Player2] = new GUI_City(0, 0, Owners::Player2, m_context);
		m_gui_cities[Owners::Player3] = new GUI_City(0, 0, Owners::Player3, m_context);
		m_gui_cities[Owners::Player4] = new GUI_City(0, 0, Owners::Player4, m_context);

		for (const auto&[city_owner, city] : m_gui_cities)
		{
			const float city_slacing_factor = .2f;
			city->set_scale(city_slacing_factor, city_slacing_factor);
		}

		m_gui_robber = new GUI_Robber(0, 0, Texture_ID::ROBBER, m_context);

		m_is_offering_button = new GUI_Is_Offering_Button(0, 0, Texture_ID::CIRCLE_BUTTON, m_context);
		m_is_offering_button->set_scale(.2f, .2f);


		/************************** obrisi, ovo je samo za testiranje **************************/

		const auto& helper_gui_hexs = m_game_state->get_board()->get_hexagons();

		// Robber

		helper_gui_hexs[5]->set_is_robber_here(true);


		/***************************************************************************************/
	}

	GUI_Hex_Grid::~GUI_Hex_Grid()
	{
		for (const auto& gui_hex : m_gui_hexagons)
		{
			delete gui_hex;
		}
		delete m_gui_houses[Owners::Player1];
		delete m_gui_houses[Owners::Player2];
		delete m_gui_houses[Owners::Player3];
		delete m_gui_houses[Owners::Player4];

		delete m_gui_cities[Owners::Player1];
		delete m_gui_cities[Owners::Player2];
		delete m_gui_cities[Owners::Player3];
		delete m_gui_cities[Owners::Player4];

		delete m_gui_robber;
		delete m_is_offering_button;
	}

	/**************** Other Methods ****************/

	float GUI_Hex_Grid::column_offset_fix_y(int row)
	{
		if (row == 1 || row == 2 || row == 3 || row == 4)
		{
			return -row * (m_first_hex_side_length / 2);
		}
		return 0;
	}

	float GUI_Hex_Grid::row_offset_fix_x(int row)
	{
		if (row == 0 || row == 4)
		{
			return 2 * (std::sqrt(3.f) * m_first_hex_side_length / 2);
		}
		if (row == 1 || row == 3)
		{
			return (std::sqrt(3.f) * m_first_hex_side_length / 2);
		}
		return 0;
	}

	void GUI_Hex_Grid::draw()
	{
		for (const auto& gui_hex : m_gui_hexagons)
		{
			gui_hex->draw();
		}
		m_game_state->get_board()->reset_nodes_for_traversal();

		for (const auto& gui_hex : m_gui_hexagons)
		{
			const auto& hex = gui_hex->get_hexagon();
			std::vector<sf::Vector2f> nodes_coordinats = gui_hex->get_nodes_coordinats();
			const auto& hex_roads = hex->get_roads();
			for (auto i = 0u; i < hex_roads.size(); i++)
			{
				auto start_node = i;
				auto end_node = (i + 1) % 6;
				auto road = hex_roads[i];
				if (road->is_selected())
				{
					sf::Color wanted_color;
					switch (road->get_owner())
					{
						case Owners::Player1:
							wanted_color = sf::Color::Blue;
							break;
						case Owners::Player2:
							wanted_color = sf::Color::Red;
							break;
						case Owners::Player3:
							wanted_color = sf::Color::Green;
							break;
						case Owners::Player4:
							wanted_color = sf::Color(255, 140, 0);
							break;
						default:
							std::cout << "GUI_Hex_Grid::draw switch Player color dont exist!" << std::endl;
							std::exit(EXIT_FAILURE);
					}
					auto GUI_road = new GUI_Line_Object(
						nodes_coordinats[start_node]
						, nodes_coordinats[end_node]
						, wanted_color
						, m_context
					);
					GUI_road->draw();
					delete GUI_road;
				}
			}
		}

		for (const auto& gui_hex : m_gui_hexagons)
		{
			const auto& hex = gui_hex->get_hexagon();
			std::vector<sf::Vector2f> nodes_coordinats = gui_hex->get_nodes_coordinats();
			const auto& hex_nodes = hex->get_nodes();
			for (auto i = 0u; i < hex_nodes.size(); i++)
			{
				const auto& node = hex_nodes[i];
				if (!node->is_visited_in_this_traversal())
				{
					node->set_is_visited_in_this_traversal(true);
//                    if (node->is_offering()) {
//                    	m_is_offering_button->set_position(nodes_coordinats[i].x, nodes_coordinats[i].y);
//                    	m_is_offering_button->draw();
//                    }
					if (node->is_house_built())
					{
						const auto& wanted_house = m_gui_houses[node->get_owner()];
						wanted_house->set_position(nodes_coordinats[i].x, nodes_coordinats[i].y);
						wanted_house->draw();
					}
					else if (node->is_hotel_built())
					{
						const auto& wanted_city = m_gui_cities[node->get_owner()];
						wanted_city->set_position(nodes_coordinats[i].x, nodes_coordinats[i].y);
						wanted_city->draw();
					}
				}
			}

			if (hex->is_robber_here())
			{
				if (hex->get_number() == 7)
				{
					m_gui_robber->place_inside(gui_hex, 25, 0);
					const float robber_scaling_factor = .45f;
					m_gui_robber->set_scale(robber_scaling_factor, robber_scaling_factor);
				}
				else
				{
					m_gui_robber->place_inside(gui_hex, 4, -22);
					const float robber_scaling_factor = .35f;
					m_gui_robber->set_scale(robber_scaling_factor, robber_scaling_factor);
				}
				m_gui_robber->draw();
			}
		}
	}

	void GUI_Hex_Grid::set_nodes_coordinates()
	{
		for (const auto& gui_hex : m_gui_hexagons)
		{
			const auto& hex = gui_hex->get_hexagon();
			std::vector<sf::Vector2f> nodes_coordinats = gui_hex->get_nodes_coordinats();
			const auto& hex_nodes = hex->get_nodes();
			for (auto i = 0u; i < hex_nodes.size(); i++)
			{
				hex_nodes[i]->set_center(nodes_coordinats[i]);
			}
		}
	}

	void GUI_Hex_Grid::set_roads_coordinates()
	{
		for (const auto& gui_hex : m_gui_hexagons)
		{
			const auto& hex = gui_hex->get_hexagon();
			hex->get_left_road()->set_center(
				hex->get_left_up_node()->get_center(), hex->get_left_bottom_node()->get_center());
			hex->get_up_left_road()->set_center(
				hex->get_left_up_node()->get_center(), hex->get_up_node()->get_center());
			hex->get_up_right_road()->set_center(
				hex->get_right_up_node()->get_center(), hex->get_up_node()->get_center());
			hex->get_right_road()->set_center(
				hex->get_right_up_node()->get_center(), hex->get_right_bottom_node()->get_center());
			hex->get_down_right_road()->set_center(
				hex->get_right_bottom_node()->get_center(), hex->get_bottom_node()->get_center());
			hex->get_down_left_road()->set_center(
				hex->get_left_bottom_node()->get_center(), hex->get_bottom_node()->get_center());
		}
	}

}// namespace catan_game