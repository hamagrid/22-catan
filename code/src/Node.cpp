#include "../include/Node.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	Node::Node(int id)
		: m_id(id)
		  , m_owner(Owners::NoOwner)
		  , m_is_house_built(false)
		  , m_is_hotel_built(false)
		  , m_is_offering(false)
		  , m_is_visited_in_this_house_setting(false)
	{
	}

	/**************** Getters ****************/

	int Node::get_id() const
	{
		return m_id;
	}

	Owners Node::get_owner() const
	{
		return m_owner;
	}

	sf::Vector2f Node::get_center()
	{
		return m_center;
	}

	bool Node::is_house_built() const
	{
		return m_is_house_built;
	}

	bool Node::is_hotel_built() const
	{
		return m_is_hotel_built;
	}

	bool Node::is_visited_in_this_traversal() const
	{
		return m_is_visited_in_this_traversal;
	}

	bool Node::is_clicked(sf::Vector2f pos)
	{
		int diff = 10;
		bool hor = m_center.x - diff < pos.x && pos.x < m_center.x + diff;
		bool ver = m_center.y - diff < pos.y && pos.y < m_center.y + diff;
		if (hor && ver)
		{ return true; }
		else
		{
			return false;
		}
	}

	bool Node::is_offering() const
	{
		return m_is_offering;
	}

	bool Node::is_visited_in_this_house_setting()
	{
		return m_is_visited_in_this_house_setting;
	}

	/**************** Setters ****************/

	void Node::set_owner(Owners value)
	{
		m_owner = value;
	}

	void Node::set_is_house_built(bool value)
	{
		m_is_house_built = value;
	}

	void Node::set_is_hotel_built(bool value)
	{
		m_is_hotel_built = value;
	}

	void Node::set_is_visited_in_this_traversal(bool value)
	{
		m_is_visited_in_this_traversal = value;
	}

	void Node::set_offering(bool value)
	{
		m_is_offering = value;
	}

	void Node::set_is_visited_in_this_house_setting(bool value)
	{
		m_is_visited_in_this_house_setting = value;
	}

	void Node::set_center(sf::Vector2f center)
	{
		m_center = center;
	}

	void Node::set_house(Owners owner)
	{
		this->set_is_house_built(true);
		this->set_owner(owner);

	}

	void Node::set_hotel(Owners owner)
	{
		this->set_is_house_built(false);
		this->set_is_hotel_built(true);
		this->set_owner(owner);

	}

	/**************** Other Methods ****************/

	bool Node::operator==(const Node& other) const
	{
		return m_id == other.m_id;
	}

	bool Node::operator!=(const Node& other) const
	{
		return !(other == *this);
	}

}// namespace catan_game