#include "../include/GUI_Is_Offering_Button.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_Is_Offering_Button::GUI_Is_Offering_Button(
		float center_x
		, float center_y
		, Texture_ID texture_id
		, Context* context
	)
		: GUI_Sprite_Object(center_x, center_y, context->m_assets->get_texture(texture_id), context)
	{
	}

}// namespace catan_game