#include "../include/Loading.hpp"

#include "../include/Menu.hpp"
#include "SFML/Graphics.hpp"

#include <iostream>
#include <memory>
#include <stdlib.h>

namespace catan_game
{

	/**************** Constructors/Destructor/AssignmentOperator ****************/

	Loading::Loading(Context* context)
		: m_context(context), m_is_play_clicked(false), m_is_quit_clicked(false)
	{
	}

	Loading::~Loading()
	{
	}

	/**************** Other Methods ****************/

	void Loading::init()
	{
		if (!m_texture_background.loadFromFile("../resources/images/catan.jpg"))
		{
			std::cout << __FILE__ << " " << "Failed loading background image" << std::endl;
			exit(EXIT_FAILURE);
		}
		m_background_image.setTexture(m_texture_background);

		if (!m_texture_play.loadFromFile("../resources/images/play.png"))
		{
			std::cout << __FILE__ << " " << "Failed loading play image" << std::endl;
			exit(EXIT_FAILURE);
		}
		m_play_image.setTexture(m_texture_play);
		m_play_image.setPosition(600, 400);

		if (!m_texture_quit.loadFromFile("../resources/images/quit.png"))
		{
			std::cout << __FILE__ << " " << "Failed loading quit image" << std::endl;
			exit(EXIT_FAILURE);
		}
		m_quit_image.setTexture(m_texture_quit);
		m_quit_image.setPosition(300, 400);
	}

	void Loading::process_input()
	{
		sf::Event event;
		while (m_context->m_window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				m_context->m_window->close();
			}

			if (event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2f
					mouse = m_context->m_window->mapPixelToCoords(sf::Mouse::getPosition(*(m_context->m_window)));
				sf::FloatRect play_bounds = m_play_image.getGlobalBounds();
				sf::FloatRect quit_bounds = m_quit_image.getGlobalBounds();

				if (play_bounds.contains(mouse))
				{
					m_is_play_clicked = true;
				}
				else if (quit_bounds.contains(mouse))
				{
					m_is_quit_clicked = true;
				}
			}
		}
	}

	void Loading::update(sf::Time deltaTime)
	{
		if (m_is_play_clicked)
		{
			m_context->m_states->replace_current(new Menu(m_context));
		}
		else if (m_is_quit_clicked)
		{
			exit(1);
		}
	}

	void Loading::draw()
	{
		m_context->m_window->clear();
		m_context->m_window->draw(m_background_image);
		m_context->m_window->draw(m_play_image);
		m_context->m_window->draw(m_quit_image);
		m_context->m_window->display();
	}

}// namespace catan_game