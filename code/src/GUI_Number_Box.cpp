#include "../include/GUI_Number_Box.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_Number_Box::GUI_Number_Box(float center_x, float center_y, int number, Context* context)
		: GUI_Sprite_Object(
		center_x, center_y, context->m_assets->get_texture(int_number_to_texture_id(number)), context
	), m_number(number)
	{
	}

	/**************** Other Methods ****************/

	Texture_ID GUI_Number_Box::int_number_to_texture_id(int number)
	{
		switch (number)
		{
			case 1:
				return Texture_ID::NUMBER_ONE_BOX;
			case 2:
				return Texture_ID::NUMBER_TWO_BOX;
			case 3:
				return Texture_ID::NUMBER_THREE_BOX;
			case 4:
				return Texture_ID::NUMBER_FOUR_BOX;
			case 5:
				return Texture_ID::NUMBER_FIVE_BOX;
			case 6:
				return Texture_ID::NUMBER_SIX_BOX;
			case 7:
				return Texture_ID::NUMBER_SEVEN_BOX;
			case 8:
				return Texture_ID::NUMBER_EIGHT_BOX;
			case 9:
				return Texture_ID::NUMBER_NINE_BOX;
			case 10:
				return Texture_ID::NUMBER_TEN_BOX;
			case 11:
				return Texture_ID::NUMBER_ELEVEN_BOX;
			case 12:
				return Texture_ID::NUMBER_TWELVE_BOX;
			default:
				std::cout << "greska GUI_Number_Box::int_number_to_texture_id" << std::endl;
				std::exit(EXIT_FAILURE);
		}
	}

}// namespace catan_game