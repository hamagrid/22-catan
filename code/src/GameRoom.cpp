#include "../include/GameRoom.hpp"

#include "../include/EndGameMenu.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GameRoom::GameRoom(Context* context)
		: m_context(context)
		  , m_game_state(new GameState(context))
		  , m_turn(1)
		  , m_first_round_finished(false)
		  , m_second_round_finished(false)
		  , m_alea_iacta_est(true)
		  , m_is_end_move(false)
		  , m_update_robber(false)
	{
		std::vector<std::pair<Texture_ID, std::string>> textures =
			{{   Texture_ID::BRICK_HEX        , "../resources/xd_exports/BrickHex.png" }
			 , { Texture_ID::WHEAT_HEX        , "../resources/xd_exports/WheatHex.png" }
			 , { Texture_ID::WOOD_HEX         , "../resources/xd_exports/WoodHex.png" }
			 , { Texture_ID::STONE_HEX        , "../resources/xd_exports/StoneHex.png" }
			 , { Texture_ID::WOOL_HEX         , "../resources/xd_exports/WoolHex.png" }
			 , { Texture_ID::DESERT_HEX       , "../resources/xd_exports/DesertHex.png" }
			 , { Texture_ID::NUMBER_ONE_BOX   , "../resources/xd_exports/1.png" }
			 , { Texture_ID::NUMBER_TWO_BOX   , "../resources/xd_exports/2.png" }
			 , { Texture_ID::NUMBER_THREE_BOX , "../resources/xd_exports/3.png" }
			 , { Texture_ID::NUMBER_FOUR_BOX  , "../resources/xd_exports/4.png" }
			 , { Texture_ID::NUMBER_FIVE_BOX  , "../resources/xd_exports/5.png" }
			 , { Texture_ID::NUMBER_SIX_BOX   , "../resources/xd_exports/6.png" }
			 , { Texture_ID::NUMBER_SEVEN_BOX , "../resources/xd_exports/7.png" }
			 , { Texture_ID::NUMBER_EIGHT_BOX , "../resources/xd_exports/8.png" }
			 , { Texture_ID::NUMBER_NINE_BOX  , "../resources/xd_exports/9.png" }
			 , { Texture_ID::NUMBER_TEN_BOX   , "../resources/xd_exports/10.png" }
			 , { Texture_ID::NUMBER_ELEVEN_BOX, "../resources/xd_exports/11.png" }
			 , { Texture_ID::NUMBER_TWELVE_BOX, "../resources/xd_exports/12.png" }
			 , { Texture_ID::ORANGE_HOUSE     , "../resources/xd_exports/OrangeHouse.png" }
			 , { Texture_ID::ORANGE_CITY      , "../resources/xd_exports/OrangeCity.png" }
			 , { Texture_ID::RED_HOUSE        , "../resources/xd_exports/RedHouse.png" }
			 , { Texture_ID::RED_CITY         , "../resources/xd_exports/RedCity.png" }
			 , { Texture_ID::BLUE_HOUSE       , "../resources/xd_exports/BlueHouse.png" }
			 , { Texture_ID::BLUE_CITY        , "../resources/xd_exports/BlueCity.png" }
			 , { Texture_ID::GREEN_HOUSE      , "../resources/xd_exports/GreenHouse.png" }
			 , { Texture_ID::GREEN_CITY       , "../resources/xd_exports/GreenCity.png" }
			 , { Texture_ID::ROBBER           , "../resources/xd_exports/Robber2.png" }
			 , { Texture_ID::CIRCLE_BUTTON    , "../resources/xd_exports/CircleButton.png" }};

		for (const auto&[texture_id, path_to_texture] : textures)
		{
			m_context->m_assets->add_texture(texture_id, path_to_texture);
		}

		m_gui_hex_grid = new GUI_Hex_Grid(m_game_state, context, 100, 140, 70);
		m_gui_widgets = new GUI_Widgets(context, m_game_state);
		m_gui_players = new GUI_Players(context);
		m_trade = new Trade(context, m_gui_players);
	}

	GameRoom::~GameRoom()
	{
		delete m_game_state;
		delete m_gui_hex_grid;
		delete m_gui_widgets;
		delete m_gui_players;
	}

	/**************** Other Methods ****************/


	void GameRoom::set_m_turn(int value)
	{
		m_turn = value;
	}

	void GameRoom::init()
	{
		m_players = m_context->m_assets->get_players();
	}

	void GameRoom::process_input()
	{
		sf::Event event{};
		while (m_context->m_window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				m_context->m_window->close();
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(m_context->m_window));
				sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(mouse_pos);

				if (!m_first_round_finished)
				{
					first_round(mouse);
				}
				else if (!m_second_round_finished)
				{
					second_round(mouse);
				}
				else if (m_update_robber)
				{
					set_robber(mouse);
				}
				else
				{
					game_loop(event, mouse);
				}
			}
			start_trade(event);
		}
	}

	void GameRoom::update(sf::Time deltaTime)
	{
	}

	void GameRoom::draw()
	{
		m_context->m_window->clear(sf::Color(30, 144, 255));
		m_gui_players->draw_players();
		m_gui_hex_grid->draw();
		m_gui_widgets->draw(m_players[m_turn]->get_name(), m_players[m_turn]->get_color());
		m_trade->draw(m_players);
		m_context->m_window->display();
	}

	void GameRoom::change_player_turn()
	{
		m_turn = m_turn + 1 > 4 ? 1 : m_turn + 1;
	}

	void GameRoom::reverse_change_player_turn()
	{
		m_turn = m_turn - 1 < 1 ? 1 : m_turn - 1;
	}

	void GameRoom::calculate_longest_road()
	{
		int longest_road = 0;
		Player* player_with_longest_road = nullptr;
		for (auto i = 1u; i < m_players.size(); i++)
		{
			const auto& player = m_players[i];
			const auto& board = m_game_state->get_board();
			player->calculate_longest_road(board);
			int current_player_longest_road = player->get_longest_road();
			if (current_player_longest_road >= 5 && current_player_longest_road > longest_road)
			{
				longest_road = current_player_longest_road;
				player_with_longest_road = player;
			}
		}

		if (player_with_longest_road == nullptr)
		{
			return;
		}

		for (auto i = 1u; i < m_players.size(); i++)
		{
			const auto& player = m_players[i];
			if (player->get_has_longest_road())
			{
				if (*player == *player_with_longest_road)
				{
					return;
				}
				else
				{
					player->decrease_victory_point();
					player->decrease_victory_point();
					player->set_has_longest_road(false);
				}
			}
		}

		for (auto i = 1u; i < m_players.size(); i++)
		{
			const auto& player = m_players[i];
			if (*player == *player_with_longest_road)
			{
				player->increase_victory_point();
				player->increase_victory_point();
				player->set_has_longest_road(true);
			}
		}
	}

	void GameRoom::first_round(sf::Vector2f& mouse)
	{
		if (m_players[4]->get_first_round_finished())
		{
			for (int i = 1; i < 5; i++)
			{
				m_players[i]->house_and_road_reset();
			}
			m_game_state->get_board()->reset_nodes_for_setting_houses();
			set_m_turn(4);
			m_first_round_finished = true;
			return;
		}
		if (!m_players[m_turn]->is_house_built())
		{
			check_house(mouse, true);
		}
		if (m_players[m_turn]->is_house_built() && !m_players[m_turn]->is_road_built())
		{
			set_roads_for_first_two_rounds(mouse, true);
		}
		if (m_players[m_turn]->get_first_round_finished())
		{
			change_player_turn();
		}
	}

	void GameRoom::second_round(sf::Vector2f& mouse)
	{
		if (m_players[1]->get_second_round_finished())
		{
			for (int i = 1; i < 5; i++)
			{
				m_players[i]->house_and_road_reset();
			}
			m_game_state->get_board()->add_resource_from_second_house(m_players, m_bank);
			m_game_state->get_board()->reset_nodes_for_setting_houses();
			set_m_turn(1);
			m_second_round_finished = true;
			calculate_longest_road();
			return;
		}
		if (!m_players[m_turn]->is_house_built())
		{
			check_house(mouse, false);
		}
		if (m_players[m_turn]->is_house_built() && !m_players[m_turn]->is_road_built())
		{
			set_roads_for_first_two_rounds(mouse, false);
		}
		if (m_players[m_turn]->get_second_round_finished())
		{
			reverse_change_player_turn();
		}
	}

	void GameRoom::check_house(sf::Vector2f mouse, bool is_first_round)
	{
		m_game_state->get_board()->update_offering_nodes();
		const auto& nodes = m_game_state->get_board()->get_nodes();
		for (const auto& n : nodes)
		{
			if (n->is_clicked(mouse) && !n->is_house_built() && n->is_offering())
			{
				n->set_house(m_players[m_turn]->get_owner());
				n->set_is_visited_in_this_house_setting(true);
				m_players[m_turn]->add_house(n);
				if (is_first_round)
				{
					m_players[m_turn]->set_first_house(n);
				}
				else
				{
					m_players[m_turn]->set_second_house(n);
				}
				return;
			}
		}
	}

	void GameRoom::set_roads_for_first_two_rounds(sf::Vector2f& mouse, bool is_first_round)
	{
		m_game_state->get_board()->set_roads(mouse, is_first_round, m_players[m_turn]);
	}

	bool GameRoom::check_city_cards()
	{
		return m_players[m_turn]->has_city_cards();
	}

	void GameRoom::set_city()
	{
		if (!m_players[m_turn]->has_place_for_city())
		{
			m_players[m_turn]->set_is_city_built(true);
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(m_context->m_window));
			sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(mouse_pos);
			for (auto& n : m_players[m_turn]->get_nodes())
			{
				if (n->is_clicked(mouse) && n->is_house_built())
				{
					if (m_players[m_turn]->has_city_cards())
					{
						m_players[m_turn]->set_city(m_bank);
						n->set_hotel(m_players[m_turn]->get_owner());
						break;
					}
				}
			}
		}
	}

	bool GameRoom::check_road_cards()
	{
		return m_players[m_turn]->has_road_cards();
	}

	void GameRoom::set_road()
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			std::vector<Road*> roads;
			std::vector<Node*> tmp_nodes;
			Owners current_owner = m_players[m_turn]->get_owner();
			for (auto& node : m_players[m_turn]->get_nodes())
			{
				auto& neighbours = m_game_state->get_board()->get_neighbours_of_node(node);
				for (auto& neighbour : neighbours)
				{
					if (!(neighbour->road->is_selected()))
					{
						roads.push_back(neighbour->road);
						tmp_nodes.push_back(neighbour->node);
					}
				}
			}

			for (auto& node : m_players[m_turn]->get_neighbour_nodes())
			{
				auto& neighbours = m_game_state->get_board()->get_neighbours_of_node(node);
				for (auto& neighbour : neighbours)
				{
					if (!(neighbour->road->is_selected()))
					{
						roads.push_back(neighbour->road);
						tmp_nodes.push_back(neighbour->node);
					}
				}
			}

			sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(m_context->m_window));
			sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(mouse_pos);
			int i = 0;
			for (const auto& road : roads)
			{
				if (road->is_clicked(mouse) && !road->is_selected())
				{
					road->set_road(current_owner);
					m_players[m_turn]->set_road(road, tmp_nodes[i], m_bank);
				}
				i++;
			}
		}
		calculate_longest_road();
	}

	bool GameRoom::check_house_cards()
	{
		return m_players[m_turn]->has_house_cards();
	}

	bool GameRoom::check_if_there_are_offering_nodes()
	{
		for (auto const& node : m_game_state->get_board()->get_nodes())
		{
			if (node->is_offering())
			{
				return true;
			}
		}
		return false;
	}

	void GameRoom::set_house()
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_game_state->get_board()->reset_offerings();
			m_game_state->get_board()->reset_offering_nodes();
			offering_nodes.clear();

			for (auto& node : m_players[m_turn]->get_nodes())
			{
				m_game_state->get_board()->dfs(node->get_id(), m_players[m_turn]);
			}

			offering_nodes = m_game_state->get_board()->get_offering_nodes();

			for (auto& it : offering_nodes)
			{
				if (!it->is_hotel_built())
				{
					it->set_offering(true);
					//std::cout << it->get_id() << " ";
				}
			}
			//std::cout << "\n";

			if (!check_if_there_are_offering_nodes())
			{
				m_players[m_turn]->set_is_house_built(true);
				return;
			}
			Owners current_owner = m_players[m_turn]->get_owner();

			sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(m_context->m_window));
			sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(mouse_pos);
			for (const auto& node : offering_nodes)
			{
				if (node->is_clicked(mouse) && node->is_offering() /* && !node->is_hotel_built() */)
				{
					node->set_house(current_owner);
					m_players[m_turn]->set_house(node, m_bank);
				}
			}
		}
	}

	void GameRoom::set_robber(sf::Vector2f& mouse)
	{
		m_game_state->get_board()->reset_robber();
		const std::vector<Hexagon*> hexagons = m_game_state->get_board()->get_hexagons();
		for (auto& it : hexagons)
		{
			if (it->is_clicked(mouse))
			{
				it->set_is_robber_here(true);
				m_update_robber = false;
				break;
			}
		}
	}

	void GameRoom::game_loop(sf::Event& event, sf::Vector2f& mouse)
	{
		start_or_finish_move(mouse);
		start_build(event, mouse);
	}

	void GameRoom::start_build(sf::Event& event, sf::Vector2f& mouse)
	{
		if (m_gui_widgets->is_plain_city_clicked(mouse) && check_city_cards())
		{
			m_players[m_turn]->set_is_city_built(false);
			while (!m_players[m_turn]->is_city_built())
			{
				set_city();
			}
		}
		if (m_gui_widgets->is_plain_house_clicked(mouse) && check_house_cards())
		{
			m_players[m_turn]->set_is_house_built(false);
			while (!m_players[m_turn]->is_house_built())
			{
				set_house();
			}
		}
		if (m_gui_widgets->is_plain_road_clicked(mouse) && check_road_cards())
		{
			m_players[m_turn]->set_is_road_built(false);
			while (!m_players[m_turn]->is_road_built())
			{
				set_road();
			}
		}
	}

	void GameRoom::start_trade(sf::Event& event)
	{
		m_gui_players->react_to_click(event);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(m_context->m_window));
			sf::Vector2f mouse = m_context->m_window->mapPixelToCoords(mouse_pos);

			if (m_trade->is_trade_clicked(mouse))
			{
				m_trade->trade(m_turn);
			}
			if (m_trade->is_trade_reset_clicked(mouse))
			{
				m_gui_players->clear_trade_value();
				std::cout << "Trade reset successful" << std::endl;
			}
		}
	}

	void GameRoom::start_or_finish_move(sf::Vector2f& mouse)
	{
		if (m_gui_widgets->is_dices_clicked(mouse) && m_alea_iacta_est)
		{
			m_alea_iacta_est = false;
			m_is_end_move = true;
			int dices = m_game_state->get_board()->move(m_players, m_bank);
			if (dices == 7)
			{
				m_update_robber = true;
			}
		}
		else if (m_gui_widgets->is_end_move_clicked(mouse) && m_is_end_move)
		{
			if (m_players[m_turn]->get_victory_points() >= 10)
			{
				m_context->m_states->replace_current(new EndGameMenu(m_players[m_turn]->get_name(), m_context));
			}
			m_is_end_move = false;
			m_alea_iacta_est = true;
			change_player_turn();
		}
	}
}// namespace catan_game
