#include <iostream>

#include "../include/Player.hpp"

namespace catan_game
{
	Player::Player()
	{
	}

	Player::Player(int id, std::string name, sf::Color color)
		: m_id(id)
		  , m_name(std::move(name))
		  , m_color(color)
		  , m_first_round_finished(false)
		  , m_second_round_finished(
			false
		)
		  , m_victory_points(0)
		  , m_num_of_houses(5)
		  , m_num_of_cities(4)
		  , m_num_of_roads(15)
		  , m_house_is_built(false)
		  , m_road_is_built(false)
		  , m_city_is_built(false)
		  , m_longest_road(0)
	{
		m_player_resource[ResourceType::Brick] = 0;
		m_player_resource[ResourceType::Stone] = 0;
		m_player_resource[ResourceType::Wheat] = 0;
		m_player_resource[ResourceType::Wood] = 0;
		m_player_resource[ResourceType::Wool] = 0;

		switch (id)
		{
			case 1:
				m_owner = Owners::Player1;
				break;
			case 2:
				m_owner = Owners::Player2;
				break;
			case 3:
				m_owner = Owners::Player3;
				break;
			case 4:
				m_owner = Owners::Player4;
				break;
			default:
				std::cout << "Error Player::Player switch" << std::endl;
				std::exit(EXIT_FAILURE);
		}
	}

	const std::string& Player::get_name() const
	{
		return m_name;
	}

	sf::Color Player::get_color() const
	{
		return m_color;
	}

	unsigned Player::get_victory_points() const
	{
		return m_victory_points;
	}

	unsigned Player::get_num_of_houses() const
	{
		return m_num_of_houses;
	}

	unsigned Player::get_num_of_cities() const
	{
		return m_num_of_cities;
	}

	unsigned Player::get_num_of_roads() const
	{
		return m_num_of_roads;
	}

	bool Player::get_first_round_finished() const
	{
		return m_first_round_finished;
	}

	bool Player::get_second_round_finished() const
	{
		return m_second_round_finished;
	}

	std::set<Node*>& Player::get_nodes()
	{
		return m_nodes;
	}

	bool Player::is_house_built() const
	{
		return m_house_is_built;
	}

	bool Player::is_road_built() const
	{
		return m_road_is_built;
	}

	Node* Player::get_first_house()
	{
		return m_first_house;
	}

	Node* Player::get_second_house()
	{
		return m_second_house;
	}

	void Player::set_is_house_built(bool value)
	{
		m_house_is_built = value;
	}

	void Player::set_is_road_built(bool value)
	{
		m_road_is_built = value;
	}

	void Player::set_first_house(Node* node)
	{
		m_first_house = node;
	}

	void Player::set_second_house(Node* node)
	{
		m_second_house = node;
	}

	bool Player::give_resource_card_to_player(ResourceType rt, int amount, Player& p)
	{
		if (m_player_resource[rt] < amount)
		{
			return false;
		}
		m_player_resource[rt] -= amount;
		auto p2_resources = p.getMPlayerResource();
		p2_resources[rt] += amount;
		p.setMPlayerResource(p2_resources);
		return true;
	}

	bool Player::take_resource_card_from_bank(ResourceType resource, int amount, Bank& bank)
	{//TODO change member functions in m_bank
		m_player_resource[resource] += amount;
		bank.take_resource_card(resource, amount);
		return true;//TODO exceptions
	}

	bool Player::add_resource_card_to_bank(ResourceType rt, int amount, Bank& bank)
	{
		m_player_resource[rt] -= amount;
		bank.add_resource_card(rt, amount);
		return true;
	}

	const std::unordered_map<ResourceType, unsigned>& Player::getMPlayerResource() const
	{
		return m_player_resource;
	}

	void Player::setMPlayerResource(const std::unordered_map<ResourceType, unsigned>& mPlayerResource)
	{
		m_player_resource = mPlayerResource;
	}

	bool Player::trade(
		ResourceType rt_player1, int amount_player1, ResourceType rt_player2, int amount_player2, Player& player2
	)
	{
		bool success_giving = give_resource_card_to_player(rt_player1, amount_player1, player2);
		bool success_taking = player2.give_resource_card_to_player(rt_player2, amount_player2, *this);
		if (success_giving && success_taking)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	Owners Player::get_owner() const
	{
		switch (m_id)
		{
			case 1:
				return Owners::Player1;
			case 2:
				return Owners::Player2;
			case 3:
				return Owners::Player3;
			case 4:
				return Owners::Player4;
		}
		return Owners::Player1;
	}

	void Player::insert_node(Node* n)
	{
		m_nodes.insert(n);
	}

	void Player::insert_road(Road* road)
	{
		m_roads.insert(road);
	}

	void Player::increase_house_number()
	{
		m_num_of_houses++;
	}

	void Player::increase_victory_point()
	{
		m_victory_points++;
	}

	void Player::decrease_house_number()
	{
		m_num_of_houses--;
	}

	void Player::decrease_city_number()
	{
		m_num_of_cities--;
	}

	void Player::decrease_road_number()
	{
		m_num_of_roads--;
	}

	void Player::decrease_victory_point()
	{
		m_victory_points--;
	}

	void Player::set_is_first_round_finished(bool value)
	{
		m_first_round_finished = value;
	}

	void Player::set_is_second_round_finished(bool value)
	{
		m_second_round_finished = value;
	}

	void Player::add_neighbour_nodes(Node* node)
	{
		m_neighbour_nodes.push_back(node);
	}

	std::vector<Node*>& Player::get_neighbour_nodes()
	{
		return m_neighbour_nodes;
	}

	std::string Player::print_map_wood()
	{
		return ":" + std::to_string(m_player_resource[ResourceType::Wood]);
	}

	std::string Player::print_map_wheat()
	{
		return ":" + std::to_string(m_player_resource[ResourceType::Wheat]);
	}

	std::string Player::print_map_brick()
	{
		return ":" + std::to_string(m_player_resource[ResourceType::Brick]);
	}

	std::string Player::print_map_stone()
	{
		return ":" + std::to_string(m_player_resource[ResourceType::Stone]);
	}

	std::string Player::print_map_wool()
	{
		return ":" + std::to_string(m_player_resource[ResourceType::Wool]);
	}

	std::string Player::print_longest_road() const
	{
		return "LR:" + std::to_string(m_longest_road);
	}

	std::string Player::print_victory_points() const
	{
		return std::to_string(m_victory_points);
	}

	bool Player::has_city_cards() const
	{
		bool cities = this->get_num_of_cities() > 0;
		bool stones = m_player_resource.at(ResourceType::Stone) >= 3;
		bool wheat = m_player_resource.at(ResourceType::Wheat) >= 2;
		return cities && stones && wheat;
	}

	bool Player::has_house_cards() const
	{
		bool houses = this->get_num_of_houses() > 0;
		bool woods = m_player_resource.at(ResourceType::Wood) >= 1;
		bool bricks = m_player_resource.at(ResourceType::Brick) >= 1;
		bool wheat = m_player_resource.at(ResourceType::Wheat) >= 1;
		bool wools = m_player_resource.at(ResourceType::Wool) >= 1;
		return houses && woods && bricks && wheat && wools;
	}

	bool Player::has_road_cards() const
	{
		bool roads = this->get_num_of_roads() > 0;
		bool woods = m_player_resource.at(ResourceType::Wood) >= 1;
		bool bricks = m_player_resource.at(ResourceType::Brick) >= 1;
		return roads && woods && bricks;
	}

	bool Player::get_has_longest_road() const
	{
		return m_has_longest_road;
	}

	void Player::set_has_longest_road(bool value)
	{
		m_has_longest_road = value;
	}

	int Player::get_longest_road() const
	{
		return m_longest_road;
	}

	void Player::calculate_longest_road(Board* board)
	{
		std::set<Node*> m_reachable_nodes;
		m_reachable_nodes.insert(m_nodes.begin(), m_nodes.end());
		m_reachable_nodes.insert(m_neighbour_nodes.begin(), m_neighbour_nodes.end());
		int longest_road = 0;
		for (auto start_node = m_reachable_nodes.begin(); start_node != m_reachable_nodes.end(); start_node++)
		{
			for (auto finish_node = start_node; finish_node != m_reachable_nodes.end(); finish_node++)
			{
				board->reset_nodes_for_traversal();
				board->reset_roads_for_traversal();
				(*start_node)->set_is_visited_in_this_traversal(true);
				longest_road = std::max(longest_road, calculate_longest_road_dfs(board, *start_node, *finish_node, 0));
			}
		}
		m_longest_road = longest_road;
	}

	int Player::calculate_longest_road_dfs(Board* board, Node* current_node, Node* finish_node, int current_length)
	{
		int max_length = 0;
		if (*current_node == *finish_node)
		{
			return current_length;
		}
		for (const auto& neighbour : board->get_neighbours_of_node(current_node))
		{
			if (!neighbour->node->is_visited_in_this_traversal() && neighbour->road->get_owner() == m_owner)
			{
				neighbour->node->set_is_visited_in_this_traversal(true);
				max_length = std::max(
					max_length, calculate_longest_road_dfs(
						board, neighbour->node, finish_node, current_length + 1
					));
				neighbour->node->set_is_visited_in_this_traversal(false);
			}
		}
		return max_length;
	}

	void Player::set_city(Bank& bank)
	{
		this->decrease_city_number();
		this->increase_house_number();
		this->increase_victory_point();
		this->add_resource_card_to_bank(ResourceType::Stone, 3, bank);
		this->add_resource_card_to_bank(ResourceType::Wheat, 2, bank);
		this->set_is_city_built(true);
	}

	bool Player::is_city_built() const
	{
		return m_city_is_built;
	}

	void Player::set_is_city_built(bool value)
	{
		m_city_is_built = value;
	}

	void Player::set_house(Node* node, Bank& bank)
	{
		this->set_is_house_built(true);
		this->insert_node(node);
		this->decrease_house_number();
		this->increase_victory_point();
		this->add_resource_card_to_bank(ResourceType::Brick, 1, bank);
		this->add_resource_card_to_bank(ResourceType::Wood, 1, bank);
		this->add_resource_card_to_bank(ResourceType::Wheat, 1, bank);
		this->add_resource_card_to_bank(ResourceType::Wool, 1, bank);
	}

	void Player::set_road(Road* road, Node* node, Bank& bank)
	{
		this->set_is_road_built(true);
		this->insert_road(road);
		this->add_neighbour_nodes(node);
		this->decrease_road_number();
		this->add_resource_card_to_bank(ResourceType::Brick, 1, bank);
		this->add_resource_card_to_bank(ResourceType::Wood, 1, bank);
	}

	void Player::add_house(Node* node)
	{
		this->insert_node(node);
		this->set_is_house_built(true);
		this->increase_victory_point();
	}

	void Player::add_road(Road* road, Node* node)
	{
		this->set_is_road_built(true);
		this->insert_road(road);
		this->add_neighbour_nodes(node);
	}

	void Player::house_and_road_reset()
	{
		this->set_is_house_built(false);
		this->set_is_road_built(false);
	}

	bool Player::has_place_for_city() const
	{
		for (auto const& node : m_nodes)
		{
			if (node->is_house_built() && !node->is_hotel_built())
			{
				return true;
			}
		}
		return false;
	}

	bool Player::operator==(const Player& other) const
	{
		return m_id == other.m_id;
	}

	bool Player::operator!=(const Player& other) const
	{
		return !(other == *this);
	}

}// namespace catan_game