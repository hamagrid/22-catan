#include "../include/GameState.hpp"

#include <algorithm>
#include <ctime>

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GameState::GameState(Context* context)
		: m_numbers_for_hexagons(generate_numbers_for_hexagons())
		  , m_types_for_hexagons(generate_types_for_hexagons())
		  , m_context(context)
	{
		m_board = new Board(m_numbers_for_hexagons, m_types_for_hexagons);
	}

	GameState::~GameState()
	{
		delete m_board;
	}

	Board* GameState::get_board() const
	{
		return m_board;
	}

	std::vector<int> GameState::generate_numbers_for_hexagons()
	{
		//std::vector<int> numbers_for_hexagons = {10, 2, 9, 12, 6, 4, 10, 9, 11, /*READ RULES PAGE 10*/7/*WARNING*/, 3, 8, 8, 3, 4, 5, 5, 6, 11}; // 7 must be desert
		std::vector<int> numbers_for_hexagons;
		//std::vector<int> array_of_hexagons = { 9, 10, 8, 12, 6, 5, 3, 11, 3, 4, 6, 4, 11, 9, 2, 8, 10, 5 };
		std::vector<int> array_of_hexagons = { 8, 10, 9, 12, 11, 5, 3, 6, 3, 4, 6, 4, 11, 9, 2, 10, 8, 5 };

		for (int i = 0; i < 19; i++)
		{
			if (m_types_for_hexagons[i] == ResourceType::Desert)
			{
				numbers_for_hexagons.push_back(7);
			}
			else
			{
				numbers_for_hexagons.push_back(array_of_hexagons.back());
				array_of_hexagons.pop_back();
			}
		}

		return numbers_for_hexagons;
	}

	std::vector<ResourceType> GameState::generate_types_for_hexagons()
	{
		std::vector<ResourceType> types_for_hexagons = {
			ResourceType::Stone, ResourceType::Wool, ResourceType::Wood, ResourceType::Wheat, ResourceType::Brick
			, ResourceType::Wool, ResourceType::Brick, ResourceType::Wheat, ResourceType::Wood, ResourceType::Desert
			, ResourceType::Wood, ResourceType::Stone, ResourceType::Wood, ResourceType::Stone, ResourceType::Wheat
			, ResourceType::Wool, ResourceType::Brick, ResourceType::Wheat, ResourceType::Wool
		};

		std::srand(std::time(0));
		std::random_shuffle(types_for_hexagons.begin(), types_for_hexagons.end());

		return types_for_hexagons;
	}

}// namespace catan_game