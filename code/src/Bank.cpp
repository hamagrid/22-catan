#include <iostream>

#include "../include/Bank.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	Bank::Bank()
	{
		m_bank_resource[ResourceType::Brick] = 19;
		m_bank_resource[ResourceType::Wool] = 19;
		m_bank_resource[ResourceType::Wood] = 19;
		m_bank_resource[ResourceType::Stone] = 19;
		m_bank_resource[ResourceType::Wheat] = 19;
	}

	/**************** Other Methods ****************/

	void Bank::print_bank()
	{
		std::cout << "Wool " << m_bank_resource[ResourceType::Wool] << std::endl;
		std::cout << "Brick " << m_bank_resource[ResourceType::Brick] << std::endl;
		std::cout << "Wood " << m_bank_resource[ResourceType::Wood] << std::endl;
		std::cout << "Stone " << m_bank_resource[ResourceType::Stone] << std::endl;
		std::cout << "Wheat " << m_bank_resource[ResourceType::Wheat] << std::endl;
		std::cout << "\n";
	}

	void Bank::add_resource_card(ResourceType rt, int cards_num)
	{
		m_bank_resource[rt] += cards_num;
	}

	void Bank::take_resource_card(ResourceType rt, int cards_num)
	{
		m_bank_resource[rt] -= cards_num;
	}

}// namespace catan_game