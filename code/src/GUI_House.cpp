#include "../include/GUI_House.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_House::GUI_House(float center_x, float center_y, Owners owner, Context* context)
		: GUI_Sprite_Object(
		center_x, center_y, context->m_assets->get_texture(owners_to_texture_id(owner)), context
	)
	{
	}

	/**************** Other Methods ****************/

	Texture_ID GUI_House::owners_to_texture_id(Owners owner)
	{
		switch (owner)
		{
			case Owners::Player1:
				return Texture_ID::BLUE_HOUSE;
			case Owners::Player2:
				return Texture_ID::RED_HOUSE;
			case Owners::Player3:
				return Texture_ID::GREEN_HOUSE;
			case Owners::Player4:
				return Texture_ID::ORANGE_HOUSE;
			default:
				std::cout << "greska GUI_House::owners_to_texture_id" << std::endl;
				std::exit(EXIT_FAILURE);
		}
	}

}// namespace catan_game