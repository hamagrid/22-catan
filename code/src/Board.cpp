#include <algorithm>

#include "../include/Board.hpp"


namespace catan_game
{
	/**************** Constructors/Destructor ****************/

	Board::Board(const std::vector<int>& numbers_for_hexagons, const std::vector<ResourceType>& types_for_hexagons)
	{
		m_current_road_id = 0;
		m_number_of_nodes = 54;
		m_adjacency_list.resize(m_number_of_nodes);
		m_nodes.resize(m_number_of_nodes);
		for (auto i = 0u; i < m_number_of_nodes; i++)
		{
			m_nodes[i] = new Node(i);// static cast ? explicit ?
		}
		connect_all_neighbour_nodes();
		construct_hexagons(numbers_for_hexagons, types_for_hexagons);
	}

	Board::~Board()
	{
		for (const auto& node : m_nodes)
		{
			delete node;
		}
		for (const auto& road : m_roads)
		{
			delete road;
		}
		for (const auto& neighbours : m_adjacency_list)
		{
			for (const auto& neighbour : neighbours)
			{
				delete neighbour;
			}
		}
		for (const auto& hexagon : m_hexagons)
		{
			delete hexagon;
		}
	}

	/**************** Getters ****************/

	int Board::get_number_of_nodes() const
	{
		return m_number_of_nodes;
	}

	const std::vector<Road*>& Board::get_roads() const
	{
		return m_roads;
	}

	const std::vector<Hexagon*>& Board::get_hexagons() const
	{
		return m_hexagons;
	}

	const std::vector<Node*>& Board::get_nodes() const
	{
		return m_nodes;
	}

	const std::vector<std::vector<Board::Neighbour*>>& Board::get_adjacency_list() const
	{
		return m_adjacency_list;
	}

	const std::pair<int, int> Board::get_dices()
	{
		return m_dices;
	}

	Player* Board::get_player_from_owner(Owners& owner, std::vector<Player*>& players)
	{
		switch (owner)
		{
			case Owners::Player1:
				return players[1];
			case Owners::Player2:
				return players[2];
			case Owners::Player3:
				return players[3];
			case Owners::Player4:
				return players[4];
			default:
				std::cerr << "Error with owner in function get_plyer_from_owner\n";
				break;
		}
		return nullptr;
	}

	const std::vector<Board::Neighbour*>& Board::get_neighbours_of_node(int node_id) const
	{
		return m_adjacency_list[node_id];
	}

	const std::vector<Board::Neighbour*>& Board::get_neighbours_of_node(Node* node) const
	{
		return get_neighbours_of_node(node->get_id());
	}

	std::set<Node*>& Board::get_offering_nodes()
	{
		return offering_nodes;
	}

	Road* Board::get_road_between(int start_node_id, int finish_node_id) const
	{
		auto start_node = m_nodes[start_node_id];
		auto finish_node = m_nodes[finish_node_id];
		return get_road_between(start_node, finish_node);
	}

	Road* Board::get_road_between(Node* start_node, Node* finish_node) const
	{
		for (auto neighbour : m_adjacency_list[start_node->get_id()])
		{
			if (neighbour->node == finish_node)
			{
				return neighbour->road;
			}
		}
		return nullptr;// handle error
	}

	/**************** Setters ****************/

	void Board::set_roads(sf::Vector2f& mouse, bool is_first_round, Player* player)
	{
		std::vector<Road*> roads;
		std::vector<Node*> nodes;
		if (is_first_round)
		{
			auto& neighbours = this->get_neighbours_of_node(player->get_first_house());
			for (auto& neighbour : neighbours)
			{
				roads.push_back(neighbour->road);
				nodes.push_back(neighbour->node);
			}
		}
		else
		{
			auto& neighbours = this->get_neighbours_of_node(player->get_second_house());
			for (auto& neighbour : neighbours)
			{
				roads.push_back(neighbour->road);
				nodes.push_back(neighbour->node);
			}
		}

		int i = 0;
		for (const auto& road : roads)
		{
			if (road->is_clicked(mouse) && !road->is_selected())
			{
				Owners owner = player->get_owner();
				road->set_road(owner);
				player->add_road(road, nodes[i]);
				if (is_first_round)
				{
					player->set_is_first_round_finished(true);
				}
				else
				{
					player->set_is_second_round_finished(true);
				}
			}
			i++;
		}

	}

	/**************** Other Methods ****************/

	void Board::connect_all_neighbour_nodes()
	{
		std::vector<std::vector<int>> m_adjacency_list_as_node_ids = {{   1 , 8 }
																	  , { 2 }
																	  , { 3 , 10 }
																	  , { 4 }
																	  , { 5 , 12 }
																	  , { 6 }
																	  , { 14 }
																	  , { 8 , 17 }
																	  , { 9 }
																	  , { 10, 19 }
																	  , { 11 }
																	  , { 12, 21 }
																	  , { 13 }
																	  , { 14, 23 }
																	  , { 15 }
																	  , { 25 }
																	  , { 17, 27 }
																	  , { 18 }
																	  , { 19, 29 }
																	  , { 20 }
																	  , { 21, 31 }
																	  , { 22 }
																	  , { 23, 33 }
																	  , { 24 }
																	  , { 25, 35 }
																	  , { 26 }
																	  , { 37 }
																	  , { 28 }
																	  , { 29, 38 }
																	  , { 30 }
																	  , { 31, 40 }
																	  , { 32 }
																	  , { 33, 42 }
																	  , { 34 }
																	  , { 35, 44 }
																	  , { 36 }
																	  , { 37, 46 }
																	  , {}
																	  , { 39 }
																	  , { 40, 47 }
																	  , { 41 }
																	  , { 42, 49 }
																	  , { 43 }
																	  , { 44, 51 }
																	  , { 45 }
																	  , { 46, 53 }
																	  , {}
																	  , { 48 }
																	  , { 49 }
																	  , { 50 }
																	  , { 51 }
																	  , { 52 }
																	  , { 53 }
																	  , {}};

		int current_node_id = 0;
		for (const auto& neighbours_for_current_node_as_ids : m_adjacency_list_as_node_ids)
		{
			Node* current_node = m_nodes[current_node_id];
			for (int neighbour_node_id : neighbours_for_current_node_as_ids)
			{
				Node* neighbour_vertex = m_nodes[neighbour_node_id];
				Road* road_between_current_and_neighbour_node = new Road(m_current_road_id++);
				m_roads.push_back(road_between_current_and_neighbour_node);

				auto neighbour1 = new Neighbour(current_node, road_between_current_and_neighbour_node);
				auto neighbour2 = new Neighbour(neighbour_vertex, road_between_current_and_neighbour_node);

				m_adjacency_list[current_node_id].push_back(neighbour2);
				m_adjacency_list[neighbour_node_id].push_back(neighbour1);
			}

			current_node_id++;
		}
	}

	void Board::reset_nodes_for_traversal()
	{
		for (auto node : m_nodes)
		{
			node->set_is_visited_in_this_traversal(false);
		}
	}

	void Board::reset_roads_for_traversal()
	{
		for (auto road : m_roads)
		{
			road->set_is_visited_in_this_traversal(false);
		}
	}

	void Board::reset_nodes_for_setting_houses()
	{
		for (auto& node : m_nodes)
		{
			node->set_is_visited_in_this_house_setting(false);
		}
	}

	void Board::dfs_print(int start_node_id)
	{
		Node* tmp = m_nodes[start_node_id];
		reset_nodes_for_traversal();
		dfs_print(tmp);
	}

	void Board::dfs_print(Node* current_vertex)
	{
		current_vertex->set_is_visited_in_this_traversal(true);
		std::cout << current_vertex->get_id() << " ";
		for (auto neighbour : m_adjacency_list[current_vertex->get_id()])
		{
			if (!neighbour->node->is_visited_in_this_traversal())
			{
				dfs_print(neighbour->node);
			}
		}
	}

	void Board::construct_hexagons(
		const std::vector<int>& numbers_for_hexagons, const std::vector<ResourceType>& types_for_hexagons
	)
	{
		std::vector<std::vector<int>> hexagons_as_node_ids = {{ 0 , 1 , 2 , 10, 9 , 8 }
															  ,     // hex00
			{                                                   2 , 3 , 4 , 12, 11, 10 }
															  ,   // hex01
			{                                                   4 , 5 , 6 , 14, 13, 12 }
															  ,   // hex02
			{                                                   7 , 8 , 9 , 19, 18, 17 }
															  ,   // hex03
			{                                                   9 , 10, 11, 21, 20, 19 }
															  , // hex04
			{                                                   11, 12, 13, 23, 22, 21 }
															  ,// hex05
			{                                                   13, 14, 15, 25, 24, 23 }
															  ,// hex06
			{                                                   16, 17, 18, 29, 28, 27 }
															  ,// hex07
			{                                                   18, 19, 20, 31, 30, 29 }
															  ,// hex08
			{                                                   20, 21, 22, 33, 32, 31 }
															  ,// hex09
			{                                                   22, 23, 24, 35, 34, 33 }
															  ,// hex10
			{                                                   24, 25, 26, 37, 36, 35 }
															  ,// hex11
			{                                                   28, 29, 30, 40, 39, 38 }
															  ,// hex12
			{                                                   30, 31, 32, 42, 41, 40 }
															  ,// hex13
			{                                                   32, 33, 34, 44, 43, 42 }
															  ,// hex14
			{                                                   34, 35, 36, 46, 45, 44 }
															  ,// hex15
			{                                                   39, 40, 41, 49, 48, 47 }
															  ,// hex16
			{                                                   41, 42, 43, 51, 50, 49 }
															  ,// hex17
			{                                                   43, 44, 45, 53, 52, 51 } // hex18
		};
		int current_hex_id = 0;
		for (const auto& hexagon_as_node_ids : hexagons_as_node_ids)
		{
			int left_up_vertex_id = hexagon_as_node_ids[0];
			int up_vertex_id = hexagon_as_node_ids[1];
			int right_up_vertex_id = hexagon_as_node_ids[2];
			int right_bottom_vertex_id = hexagon_as_node_ids[3];
			int bottom_vertex_id = hexagon_as_node_ids[4];
			int left_bottom_vertex_id = hexagon_as_node_ids[5];

			auto left_up_vertex = m_nodes[left_up_vertex_id];
			auto up_vertex = m_nodes[up_vertex_id];
			auto right_up_vertex = m_nodes[right_up_vertex_id];
			auto right_bottom_vertex = m_nodes[right_bottom_vertex_id];
			auto bottom_vertex = m_nodes[bottom_vertex_id];
			auto left_bottom_vertex = m_nodes[left_bottom_vertex_id];

			auto current_hexagon = new Hexagon(
				current_hex_id
				, left_up_vertex
				, up_vertex
				, right_up_vertex
				, right_bottom_vertex
				, bottom_vertex
				, left_bottom_vertex
				, get_road_between(left_up_vertex, up_vertex)
				, get_road_between(up_vertex, right_up_vertex)
				, get_road_between(right_up_vertex, right_bottom_vertex)
				, get_road_between(right_bottom_vertex, bottom_vertex)
				, get_road_between(bottom_vertex, left_bottom_vertex)
				, get_road_between(left_bottom_vertex, left_up_vertex)
				, types_for_hexagons[current_hex_id]
				, numbers_for_hexagons[current_hex_id]
			);
			m_hexagons.push_back(current_hexagon);
			current_hex_id++;
		}
	}

	void Board::update_offering_nodes()
	{
		for (auto& node : m_nodes)
		{
			if (!node->is_house_built())
			{
				const std::vector<Neighbour*> current_neighbours = get_neighbours_of_node(node);
				bool neighbour_have_house = false;
				for (const auto& it : current_neighbours)
				{
					if (it->node->is_house_built())
					{
						neighbour_have_house = true;
						break;
					}
				}
				if (!neighbour_have_house)
				{
					node->set_offering(true);
				}
				else
				{
					node->set_offering(false);
				}
			}
		}
	}

	std::pair<int, int> Board::roll_dices()
	{
		srand(time(0));
		int dice1, dice2;
		dice1 = rand() % 6 + 1;
		dice2 = rand() % 6 + 1;
		return std::make_pair(dice1, dice2);
	}

	int Board::move(std::vector<Player*>& players, Bank& bank)
	{
		m_dices = roll_dices();
		int sum_of_dices = m_dices.first + m_dices.second;
		if (sum_of_dices == 7)
		{
			return 7;
		}

		const std::vector<Hexagon*> hexagons = get_hexagons();
		for (auto& it : hexagons)
		{
			if (it->get_number() == sum_of_dices && !it->is_robber_here())
			{
				ResourceType resource = it->get_type();
				const std::vector<Node*> nodes = it->get_nodes();
				for (auto& node : nodes)
				{
					if (node->is_house_built())
					{
						Owners owner = node->get_owner();
						Player* player = get_player_from_owner(owner, players);
						player->take_resource_card_from_bank(resource, 1, bank);
					}
					if (node->is_hotel_built())
					{
						Owners owner = node->get_owner();
						Player* player = get_player_from_owner(owner, players);
						player->take_resource_card_from_bank(resource, 2, bank);

					}
				}
			}
		}
		return sum_of_dices;
	}

	void Board::add_resource_from_second_house(std::vector<Player*>& players, Bank& bank)
	{
		const std::vector<Hexagon*> hexagons = get_hexagons();
		for (auto& it : hexagons)
		{
			ResourceType resource = it->get_type();
			const std::vector<Node*> nodes = it->get_nodes();
			for (auto& node : nodes)
			{
				if (node->is_house_built() && node->is_visited_in_this_house_setting())
				{
					Owners owner = node->get_owner();
					Player* player = get_player_from_owner(owner, players);
					player->take_resource_card_from_bank(resource, 1, bank);
				}
			}
		}
	}

	void Board::reset_offerings()
	{
		for (auto& node : this->get_nodes())
		{
			node->set_offering(false);
		}
	}

	void Board::check_neighbour(Node* node)
	{
		if (!node->is_house_built() && !node->is_hotel_built())
		{
			const auto& current_neighbours = this->get_neighbours_of_node(node);
			bool neighbour_have_house = false;
			for (const auto& it : current_neighbours)
			{
				if (it->node->is_house_built() || it->node->is_hotel_built())
				{
					neighbour_have_house = true;
					break;
				}
			}
			if (neighbour_have_house)
			{
				node->set_offering(false);
			}
			else
			{
				node->set_offering(true);
			}
		}
		else
		{
			node->set_offering(false);
		}
	}

	void Board::dfs(int start_node_id, Player* player)
	{
		Node* tmp = this->get_nodes()[start_node_id];
		this->reset_nodes_for_traversal();
		dfs(tmp, 0, player);
	}

	void Board::dfs(Node* current_node, int counter, Player* player)
	{
		if (counter > 1)
		{
			check_neighbour(current_node);
			if (current_node->is_offering())
			{
				offering_nodes.insert(current_node);
			}
		}

		Owners current_owner = player->get_owner();
		current_node->set_is_visited_in_this_traversal(true);
		for (auto neighbour : this->get_neighbours_of_node(current_node))
		{
			if (neighbour->road->get_owner() == current_owner && !neighbour->node->is_visited_in_this_traversal())
			{
				dfs(neighbour->node, counter + 1, player);
			}
		}
	}

	void Board::reset_offering_nodes()
	{
		offering_nodes.clear();
	}

	void Board::reset_robber()
	{
		const std::vector<Hexagon*> hexagons = get_hexagons();
		for (auto& it : hexagons)
		{
			it->set_is_robber_here(false);
		}

	}

}// namespace catan_game