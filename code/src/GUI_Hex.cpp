#include <cmath>
#include <iostream>

#include "../include/GUI_Hex.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_Hex::GUI_Hex(
		float center_x
		, float center_y
		, ResourceType type
		, int number
		, float side_length
		, Context* context
		, Hexagon* hexagon
	)
		: GUI_Sprite_Object(
		center_x, center_y, context->m_assets->get_texture(resource_type_to_texture_id(type)), context
	)
		  , m_type(type)
		  , m_number(number)
		  , m_side_length(side_length)
		  , m_triangle_height(std::sqrt(3.f) * side_length / 2)
		  , m_hexagon(hexagon)
	{
		const auto&[actual_width, actual_height] = m_texture->getSize();
		set_scale(2 * m_triangle_height / actual_width, 2 * m_side_length / actual_height);
		m_number_box = new GUI_Number_Box(0, 0, m_number, context);
		m_number_box->set_scale(.4, .4);
		m_number_box->place_inside(this, 2, 22);
		sf::Vector2f v(center_x, center_y);
		m_hexagon->set_center(v);
	}

	GUI_Hex::~GUI_Hex()
	{
		delete m_number_box;
	}

	/**************** Getters ****************/
	Hexagon* GUI_Hex::get_hexagon() const
	{
		return m_hexagon;
	}

	/**************** Other Methods ****************/

	Texture_ID GUI_Hex::resource_type_to_texture_id(ResourceType rt)
	{
		switch (rt)
		{
			case ResourceType::Brick:
				return Texture_ID::BRICK_HEX;
			case ResourceType::Wool:
				return Texture_ID::WOOL_HEX;
			case ResourceType::Wood:
				return Texture_ID::WOOD_HEX;
			case ResourceType::Stone:
				return Texture_ID::STONE_HEX;
			case ResourceType::Wheat:
				return Texture_ID::WHEAT_HEX;
			case ResourceType::Desert:
				return Texture_ID::DESERT_HEX;
			default:
				std::cout << "greska GUI_Hex::resource_type_to_texture_id" << std::endl;
				std::exit(EXIT_FAILURE);
		}
	}

	void GUI_Hex::draw()
	{
		GUI_Sprite_Object::draw();
		if (m_number == 7)
		{
			return;
		}
		m_number_box->draw();
	}

	std::vector<sf::Vector2f> GUI_Hex::get_nodes_coordinats()
	{
		// left_up, up, right_up, right_bottom, bottom, left_bottom
		std::vector<sf::Vector2f> node_coordinats = {{
														 m_center.x - m_triangle_height, m_center.y - m_side_length / 2
													 }
													 , { m_center.x                    , m_center.y - m_side_length }
													 , {
														 m_center.x + m_triangle_height, m_center.y - m_side_length / 2
			}
													 , {
														 m_center.x + m_triangle_height, m_center.y + m_side_length / 2
			}
													 , { m_center.x                    , m_center.y + m_side_length }
													 , {
														 m_center.x - m_triangle_height, m_center.y + m_side_length / 2
			}};

		return node_coordinats;
	}

}// namespace catan_game