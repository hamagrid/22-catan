#include "../include/GUI_Sprite_Object.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_Sprite_Object::GUI_Sprite_Object(float center_x, float center_y, sf::Texture* texture, Context* context)
		: GUI_Object(center_x, center_y), m_texture(texture), m_context(context)
	{
		m_sprite.setTexture(*m_texture);
		set_origin_to_center();
		m_sprite.setPosition(m_center.x, m_center.y);
	}

	/**************** Other Methods ****************/

	void GUI_Sprite_Object::draw()
	{
		auto win = m_context->m_window;
		win->draw(m_sprite);
	}

	void GUI_Sprite_Object::set_origin_to_center()
	{
		auto local_bounds = m_sprite.getLocalBounds();
		auto offset_x = local_bounds.width / 2;
		auto offset_y = local_bounds.height / 2;
		auto up_left_vertex_x = local_bounds.left;
		auto up_left_vertex_y = local_bounds.top;
		m_sprite.setOrigin(up_left_vertex_x + offset_x, up_left_vertex_y + offset_y);
	}

	void GUI_Sprite_Object::set_scale(float x_scaling_factor, float y_scaling_factor)
	{
		m_sprite.setScale(x_scaling_factor, y_scaling_factor);
	}

	void GUI_Sprite_Object::set_position(float x, float y)
	{
		m_sprite.setPosition(x, y);
	}

	void GUI_Sprite_Object::place_inside(GUI_Sprite_Object* other, float offset_x = 0, float offset_y = 0)
	{
		const auto&[other_x, other_y] = other->m_sprite.getPosition();
		m_sprite.setPosition(other_x + offset_x, other_y + offset_y);
	}

}// namespace catan_game