#include <SFML/Graphics.hpp>

#include "../include/Board.hpp"
#include "../include/Game.hpp"

int main()
{
	catan_game::Game game;
	game.run();
	return 0;
}
