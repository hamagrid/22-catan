#include "../include/GUI_City.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	GUI_City::GUI_City(float center_x, float center_y, Owners owner, Context* context)
		: GUI_Sprite_Object(
		center_x, center_y, context->m_assets->get_texture(owners_to_texture_id(owner)), context
	)
	{
	}

	/**************** Other Methods ****************/

	Texture_ID GUI_City::owners_to_texture_id(Owners owner)
	{
		switch (owner)
		{
			case Owners::Player1:
				return Texture_ID::BLUE_CITY;
			case Owners::Player2:
				return Texture_ID::RED_CITY;
			case Owners::Player3:
				return Texture_ID::GREEN_CITY;
			case Owners::Player4:
				return Texture_ID::ORANGE_CITY;
			default:
				std::cout << "greska GUI_City::owners_to_texture_id" << std::endl;
				std::exit(EXIT_FAILURE);
		}
	}

}// namespace catan_game