#include "../include/Road.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	Road::Road(int id)
		: m_owner(Owners::NoOwner), m_id(id), m_selected(false)
	{
	}

	/**************** Getters ****************/

	int Road::get_id() const
	{
		return m_id;
	}

	Owners Road::get_owner() const
	{
		return m_owner;
	}

	bool Road::is_visited_in_this_traversal() const
	{
		return m_is_visited_in_this_traversal;
	}

	sf::Vector2f Road::get_center() const
	{
		return m_center;
	}

	/**************** Setters ****************/

	void Road::set_owner(Owners value)
	{
		m_owner = value;
	}

	void Road::set_center(sf::Vector2f v1, sf::Vector2f v2)
	{
		float x = (v1.x + v2.x) / 2;
		float y = (v1.y + v2.y) / 2;
		m_center.x = x;
		m_center.y = y;
	}

	void Road::set_is_visited_in_this_traversal(bool value)
	{
		m_is_visited_in_this_traversal = value;
	}

	void Road::set_road(Owners owner)
	{
		this->select_road();
		this->set_owner(owner);

	}

	/**************** Other Methods ****************/

	bool Road::is_clicked(sf::Vector2f pos)
	{
		int diff = 10;
		bool hor = m_center.x - diff < pos.x && pos.x < m_center.x + diff;
		bool ver = m_center.y - diff < pos.y && pos.y < m_center.y + diff;
		if (hor && ver)
        { return true; }
		else
        {
            return false;
        }
	}

	bool Road::is_selected()
	{
		return m_selected;
	}

	void Road::select_road()
	{
		m_selected = true;
	}

}// namespace catan_game