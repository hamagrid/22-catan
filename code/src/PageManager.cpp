#include <iostream>

#include "../include/PageManager.hpp"

namespace catan_game
{
	/**************** Constructors/Destructor/AssignmentOperator ****************/

	PageManager::PageManager()
	{
		reset_flags();
	}

	PageManager::~PageManager()
	{
		while (!m_state_stack.empty())
		{
			delete m_state_stack.top();
			m_state_stack.pop();
		}
	}

	/**************** Other Methods ****************/

	void PageManager::add(State* new_state)
	{
		m_should_add_new_state = true;
		m_new_state = new_state;
	}

	void PageManager::replace_current(State* new_state)
	{
		add(new_state);
		m_should_replace_current_state = true;
	}

	void PageManager::pop_current()
	{
		m_should_remove_current_state = true;
	}

	void PageManager::process_state_change()
	{
		if (m_should_remove_current_state && (!m_state_stack.empty()))
		{
			delete m_state_stack.top();
			m_state_stack.pop();

			if (!m_state_stack.empty())
			{
				m_state_stack.top()->start();
			}
		}

		if (m_should_add_new_state)
		{
			if (m_should_replace_current_state && (!m_state_stack.empty()))
			{
				delete m_state_stack.top();
				m_state_stack.pop();
			}

			if (!m_state_stack.empty())
			{
				m_state_stack.top()->pause();
			}

			if (m_new_state == nullptr)
			{
				std::cout << __FILE__ << " " << "m_new_state is nullptr" << std::endl;
			}

			m_state_stack.push(m_new_state);
			m_state_stack.top()->init();
			m_state_stack.top()->start();
		}

		reset_flags();
	}

	State* PageManager::get_current()
	{
		return m_state_stack.top();
	}

	void PageManager::reset_flags()
	{
		m_new_state = nullptr;
		m_should_add_new_state = false;
		m_should_remove_current_state = false;
		m_should_replace_current_state = false;
	}

}// namespace catan_game