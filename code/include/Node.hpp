#ifndef CATAN_GAME_NODE_HPP
#define CATAN_GAME_NODE_HPP

#include <memory>
#include <vector>

#include "SFML/Graphics.hpp"

#include "Owners.hpp"

namespace catan_game
{
	class Node
	{
	private:
		/**************** Attributes ****************/
		int m_id;
		Owners m_owner;
		bool m_is_house_built;
		bool m_is_hotel_built;
		bool m_is_visited_in_this_traversal;
		bool m_is_offering;

		bool m_is_visited_in_this_house_setting;
		sf::Vector2f m_center;

	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		explicit Node(int id);
		Node(const Node&) = delete;
		Node& operator=(const Node&) = delete;

		/**************** Getters ****************/
		int get_id() const;
		Owners get_owner() const;
		bool is_house_built() const;
		bool is_hotel_built() const;
		bool is_visited_in_this_traversal() const;
		bool is_clicked(sf::Vector2f);
		bool is_offering() const;
		bool is_visited_in_this_house_setting();
		sf::Vector2f get_center();

		/**************** Setters ****************/
		void set_owner(Owners value);
		void set_is_house_built(bool value);
		void set_is_hotel_built(bool value);
		void set_is_visited_in_this_traversal(bool value);
		void set_center(sf::Vector2f);
		void set_offering(bool value);
		void set_is_visited_in_this_house_setting(bool value);
		void set_house(Owners owner);
		void set_hotel(Owners owner);

		/**************** Other Methods ****************/
		bool operator==(const Node& other) const;
		bool operator!=(const Node& other) const;
	};

}// namespace catan_game

#endif//CATAN_GAME_NODE_HPP