#ifndef CATAN_GAME_PLAYER_HPP
#define CATAN_GAME_PLAYER_HPP

#include <cstdlib>
#include <ctime>
#include <set>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "../include/Bank.hpp"
#include "Board.hpp"
#include "Node.hpp"
#include "Owners.hpp"
#include "ResourceType.hpp"
#include "Road.hpp"

namespace catan_game
{
	class Board;

	class Player
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		Player();
		Player(int id, std::string name, sf::Color);
		Player(const Player&) = delete;
		Player& operator=(const Player&) = delete;

		/**************** Getters ****************/
		const std::string& get_name() const;
		sf::Color get_color() const;
		Owners get_owner() const;
		unsigned get_victory_points() const;
		unsigned get_num_of_houses() const;
		unsigned get_num_of_cities() const;
		unsigned get_num_of_roads() const;
		bool get_first_round_finished() const;
		bool get_second_round_finished() const;
		bool get_has_longest_road() const;
		int get_longest_road() const;
		std::set<Node*>& get_nodes();
		std::vector<Node*>& get_neighbour_nodes();
		Node* get_first_house();
		Node* get_second_house();

		/**************** Setters ****************/
		void set_is_first_round_finished(bool value);
		void set_is_second_round_finished(bool value);
		void set_is_house_built(bool value);
		void set_is_road_built(bool value);
		void set_is_city_built(bool value);
		void set_first_house(Node* node);
		void set_second_house(Node* node);
		void set_has_longest_road(bool value);
		void set_city(Bank& bank);
		void set_house(Node* node, Bank& bank);
		void set_road(Road* road, Node* node, Bank& bank);

		/**************** Other Methods ****************/
		bool trade(
			ResourceType rt_player1, int amount_player1, ResourceType rt_player2, int amount_player2, Player& player2
		);
		bool give_resource_card_to_player(ResourceType rt, int amount, Player& p);
		bool take_resource_card_from_bank(ResourceType rt, int amount, Bank& bank);

		bool add_resource_card_to_bank(ResourceType rt, int amount, Bank& bank);
		void add_neighbour_nodes(Node* node);
		void add_house(Node* node);
		void add_road(Road* road, Node* node);

		const std::unordered_map<ResourceType, unsigned>& getMPlayerResource() const;
		void setMPlayerResource(const std::unordered_map<ResourceType, unsigned>& mPlayerResource);
		std::string print_map_wool();
		std::string print_map_stone();
		std::string print_map_brick();
		std::string print_map_wheat();
		std::string print_map_wood();
		std::string print_longest_road() const;
		std::string print_victory_points() const;

		void insert_node(Node*);
		void insert_road(Road*);

		void increase_house_number();
		void increase_victory_point();

		void decrease_house_number();
		void decrease_city_number();
		void decrease_road_number();
		void decrease_victory_point();

		bool is_house_built() const;
		bool is_road_built() const;
		bool is_city_built() const;
		bool has_city_cards() const;
		bool has_house_cards() const;
		bool has_road_cards() const;
		bool has_place_for_city() const;

		void calculate_longest_road(Board* board);
		int calculate_longest_road_dfs(Board* board, Node* current_node, Node* finish_node, int current_length);
		void house_and_road_reset();

		bool operator==(const Player& other) const;
		bool operator!=(const Player& other) const;

	private:
		/**************** Attributes ****************/
		int m_id;
		std::string m_name;
		sf::Color m_color;
		bool m_has_longest_road = false;
		int m_longest_road;
		std::unordered_map<ResourceType, unsigned> m_player_resource;
		std::set<Node*> m_nodes;
		std::vector<Node*> m_neighbour_nodes;
		Node* m_first_house;
		Node* m_second_house;
		std::set<Road*> m_roads;
		unsigned m_victory_points;
		unsigned m_num_of_houses;
		unsigned m_num_of_cities;
		unsigned m_num_of_roads;
		bool m_first_round_finished;
		bool m_second_round_finished;
		bool m_house_is_built;
		bool m_road_is_built;
		bool m_city_is_built;
		Owners m_owner;
	};

}// namespace catan_game

#endif//CATAN_GAME_PLAYER_HPP
