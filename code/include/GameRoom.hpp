#ifndef CATAN_GAME_GAMEROOM_HPP
#define CATAN_GAME_GAMEROOM_HPP

#include <vector>

#include <SFML/Graphics.hpp>

#include "GUI_Hex.hpp"
#include "GUI_Hex_Grid.hpp"
#include "GUI_Players.hpp"
#include "GUI_Widgets.hpp"
#include "Game.hpp"
#include "GameState.hpp"
#include "Player.hpp"
#include "State.hpp"
#include "Trade.hpp"

namespace catan_game
{
	class GameRoom : public State
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GameRoom(Context* context);
		~GameRoom();
		GameRoom(const GameRoom&) = delete;
		GameRoom& operator=(const GameRoom&) = delete;

		/**************** Other Methods ****************/
		void init() override;
		void process_input() override;
		void update(sf::Time deltaTime) override;
		void draw() override;
		std::set<Node*> offering_nodes;

	private:
		/**************** Attributes ****************/
		Context* m_context;
		std::vector<Player*> m_players;
		GameState* m_game_state;
		GUI_Hex_Grid* m_gui_hex_grid;
		GUI_Widgets* m_gui_widgets;
		GUI_Players* m_gui_players;
		Trade* m_trade;
		Bank m_bank;
		int m_turn;
		bool m_first_round_finished;
		bool m_second_round_finished;
		bool m_alea_iacta_est;
		bool m_is_end_move;
		bool m_update_robber;

		/**************** Setters ****************/
		void set_road();
		void set_house();
		void set_city();
		void set_robber(sf::Vector2f&);
		void set_roads_for_first_two_rounds(sf::Vector2f&, bool is_first_round);
		void set_m_turn(int value);

		/**************** Other Methods ****************/
		void first_round(sf::Vector2f&);
		void second_round(sf::Vector2f&);
		void game_loop(sf::Event&, sf::Vector2f&);
		void start_or_finish_move(sf::Vector2f&);
		void start_build(sf::Event&, sf::Vector2f&);
		void start_trade(sf::Event&);

		void change_player_turn();
		void reverse_change_player_turn();
		void calculate_longest_road();

		void check_house(sf::Vector2f mouse, bool is_first_round);
		bool check_city_cards();
		bool check_road_cards();
		bool check_house_cards();
		bool check_if_there_are_offering_nodes();
	};
}// namespace catan_game

#endif//CATAN_GAME_GAMEROOM_HPP
