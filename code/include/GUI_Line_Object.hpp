#ifndef CATAN_GAME_CODE_INCLUDE_GUI_LINE_OBJECT_HPP
#define CATAN_GAME_CODE_INCLUDE_GUI_LINE_OBJECT_HPP

#include <cmath>

#include <SFML/Graphics/Export.hpp>
#include <SFML/Graphics/Shape.hpp>

#include "Game.hpp"

namespace catan_game
{
	class GUI_Line_Object
	{
	public:
		GUI_Line_Object(const sf::Vector2f& point1, const sf::Vector2f& point2, sf::Color color, Context* context);
		void draw() const;

	private:
		sf::Vertex m_vertices[4];
		float m_thickness;
		sf::Color m_color;
		Context* m_context;
	};
}// namespace catan_game

#endif//CATAN_GAME_CODE_INCLUDE_GUI_LINE_OBJECT_HPP
