#ifndef CATAN_GAME_ROAD_HPP
#define CATAN_GAME_ROAD_HPP

#include "Node.hpp"

namespace catan_game
{
	class Road
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		explicit Road(int id);
		Road(const Road&) = delete;
		Road& operator=(const Road&) = delete;

		/**************** Getters ****************/
		int get_id() const;
		Owners get_owner() const;
		sf::Vector2f get_center() const;
		bool is_visited_in_this_traversal() const;

		/**************** Setters ****************/
		void set_owner(Owners value);
		void set_center(sf::Vector2f, sf::Vector2f);
		void set_is_visited_in_this_traversal(bool value);
		void set_road(Owners owner);

		/**************** Other Methods ****************/
		bool is_clicked(sf::Vector2f);
		bool is_selected();
		void select_road();

	private:
		/**************** Attributes ****************/
		int m_id;
		Owners m_owner;
		sf::Vector2f m_center;
		bool m_selected;
		bool m_is_visited_in_this_traversal;
	};

}// namespace catan_game

#endif//CATAN_GAME_ROAD_HPP