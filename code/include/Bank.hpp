#ifndef CATAN_GAME_BANK_HPP
#define CATAN_GAME_BANK_HPP

#include <unordered_map>

#include "../include/ResourceType.hpp"

namespace catan_game
{
	class Bank
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		Bank();
		Bank(const Bank&) = delete;
		Bank& operator=(const Bank&) = delete;

		/**************** Other Methods ****************/
		void print_bank();
		void add_resource_card(ResourceType, int cards_num);
		void take_resource_card(ResourceType, int cards_num);

	private:
		/**************** Attributes ****************/
		std::unordered_map<ResourceType, unsigned> m_bank_resource;

	};

}// namespace catan_game

#endif//CATAN_GAME_BANK_HPP
