#ifndef CATAN_GAME_CODE_INCLUDE_ENDGAMEMENU_HPP
#define CATAN_GAME_CODE_INCLUDE_ENDGAMEMENU_HPP

#include "State.hpp"
#include "Game.hpp"

namespace catan_game
{
	class EndGameMenu : public State
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		EndGameMenu(const std::string& name, Context* context);
		~EndGameMenu();
		EndGameMenu(const EndGameMenu&) = delete;
		EndGameMenu& operator=(const EndGameMenu&) = delete;

		/**************** Other Methods ****************/
		void init() override;
		void process_input() override;
		void update(sf::Time deltaTime) override;
		void draw() override;

	private:
		Context* m_context;
		sf::Text m_text_end_game_message;
		sf::Text m_text_press_any_key;
		sf::Font m_font;
		bool m_should_return_to_loading_page;
		sf::Texture m_texture_background;
		sf::Sprite m_background_image;
	};
}

#endif //CATAN_GAME_CODE_INCLUDE_ENDGAMEMENU_HPP
