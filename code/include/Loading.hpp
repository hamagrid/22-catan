#ifndef CATAN_GAME_LOADING_HPP
#define CATAN_GAME_LOADING_HPP

#include <SFML/Graphics.hpp>

#include "Game.hpp"
#include "State.hpp"

namespace catan_game
{
	class Loading : public State
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		Loading(Context* context);
		~Loading();
		Loading(const Loading&) = delete;
		Loading& operator=(const Loading&) = delete;

		/**************** Other Methods ****************/
		void init() override;
		void process_input() override;
		void update(sf::Time deltaTime) override;
		void draw() override;

	private:
		/**************** Attributes ****************/
		Context* m_context;
		sf::Texture m_texture_background;
		sf::Sprite m_background_image;
		sf::Texture m_texture_play;
		sf::Sprite m_play_image;
		sf::Texture m_texture_quit;
		sf::Sprite m_quit_image;
		bool m_is_play_clicked;
		bool m_is_quit_clicked;
	};
}// namespace catan_game

#endif//CATAN_GAME_LOADING_HPP
