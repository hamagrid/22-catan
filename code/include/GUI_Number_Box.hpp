#ifndef CATAN_GAME_GUI_NUMBER_BOX_HPP
#define CATAN_GAME_GUI_NUMBER_BOX_HPP

#include "GUI_Sprite_Object.hpp"
#include "Game.hpp"
#include "Texture_ID.hpp"

namespace catan_game
{
	class GUI_Number_Box : public GUI_Sprite_Object
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GUI_Number_Box(float center_x, float center_y, int number, Context* context);
		GUI_Number_Box(const GUI_Number_Box&) = delete;
		GUI_Number_Box& operator=(const GUI_Number_Box&) = delete;

		/**************** Other Methods ****************/
		Texture_ID int_number_to_texture_id(int number);

	private:
		/**************** Attributes ****************/
		int m_number;
	};

}// namespace catan_game

#endif//CATAN_GAME_GUI_NUMBER_BOX_HPP
