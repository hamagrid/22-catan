#ifndef CATAN_GAME_MENU_HPP
#define CATAN_GAME_MENU_HPP

#include <vector>

#include "SFML/Graphics.hpp"

#include "Game.hpp"
#include "Player.hpp"

namespace catan_game
{
	class Menu : public State
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		Menu(Context* context);
		~Menu();
		Menu(const Menu&) = delete;
		Menu& operator=(const Menu&) = delete;

		/**************** Other Methods ****************/
		void init() override;
		void process_input() override;
		void update(sf::Time deltaTime) override;
		void draw() override;

	private:
		/**************** Attributes ****************/
		Context* m_context;
		sf::Texture m_texture_board;
		sf::Sprite m_board_image;
		sf::Font m_font;
		struct m_player_field
		{
			sf::Text m_id;
			sf::String m_input_name;
			sf::Text m_name;
			sf::RectangleShape m_label_name;
		};
		std::vector<m_player_field> m_fields;
		std::vector<Player*> m_players;
		bool m_has_selected_field;
		int m_selected_field;
		bool m_input_finished;

		/**************** Const ****************/
		const int CHAR_SIZE = 50;
		const sf::Vector2f LABEL_SIZE = sf::Vector2f(350, 50);
		const int Y1 = 100;
		const int Y2 = 250;
		const int Y3 = 400;
		const int Y4 = 550;
		const int X_ID = 540;
		const int X_LABEL = 600;
		const sf::Color GREY = sf::Color(211, 211, 211);

		/**************** Other Methods ****************/
		void init_fields();
		void init_header(int id, const std::string& name, sf::Color color, sf::Vector2i pos);
		void init_label(int id, sf::Vector2i pos);
		void select_field(int num_field);
		void unselect_field();
		void draw_menu();
		void read_name(int id, char c);
		void is_input_finished();
		int get_clicked_field(sf::Vector2f mouse);
	};
}// namespace catan_game

#endif//CATAN_GAME_MENU_HPP
