#ifndef CATAN_GAME_PAGEMANAGER_HPP
#define CATAN_GAME_PAGEMANAGER_HPP

#include <stack>

#include "State.hpp"

namespace catan_game
{
	class PageManager
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		PageManager();
		~PageManager();
		PageManager(const PageManager&) = delete;
		PageManager& operator=(const PageManager&) = delete;

		/**************** Other Methods ****************/
		void add(State* new_state);
		void replace_current(State* new_state);
		void pop_current();
		void process_state_change();
		State* get_current();
		void reset_flags();

	private:
		/**************** Attributes ****************/
		std::stack<State*> m_state_stack;
		State* m_new_state;
		bool m_should_add_new_state;
		bool m_should_remove_current_state;
		bool m_should_replace_current_state;
	};

}// namespace catan_game

#endif//CATAN_GAME_PAGEMANAGER_HPP
