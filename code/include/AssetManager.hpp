#ifndef CATAN_GAME_ASSETMANAGER_HPP
#define CATAN_GAME_ASSETMANAGER_HPP

#include <cmath>
#include <map>
#include <string>

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Clock.hpp>

#include "Player.hpp"
#include "Texture_ID.hpp"

namespace catan_game
{
	class AssetManager
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		AssetManager();
		~AssetManager();
		AssetManager(const AssetManager&) = delete;
		AssetManager& operator=(const AssetManager&) = delete;

		/**************** Getters ****************/
		sf::Texture* get_texture(Texture_ID id) const;
		const sf::Font& get_font(int id) const;
		std::vector<Player*>& get_players();

		/**************** Other Methods ****************/
		void add_texture(Texture_ID id, const std::string& filePath);
		void add_font(int id, const std::string& filePath);
		void create_players(std::vector<Player*>& players);

	private:
		/**************** Attributes ****************/
		std::map<Texture_ID, sf::Texture*> m_textures;
		std::map<int, sf::Font*> m_fonts;
		std::vector<Player*> m_players;
	};
}// namespace catan_game

#endif//CATAN_GAME_ASSETMANAGER_HPP
