#ifndef CATAN_GAME_STATE_HPP
#define CATAN_GAME_STATE_HPP

#include <SFML/System/Time.hpp>

namespace catan_game
{
	class State
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		State()
		{
		};

		virtual ~State()
		{
		};
		State(const State&) = delete;
		State& operator=(const State&) = delete;

		/**************** Other Methods ****************/
		virtual void init() = 0;
		virtual void process_input() = 0;
		virtual void update(sf::Time delta_time) = 0;
		virtual void draw() = 0;

		virtual void pause()
		{
		};

		virtual void start()
		{
		};
	};

}// namespace catan_game

#endif//CATAN_GAME_STATE_HPP
