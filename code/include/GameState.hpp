#ifndef CATAN_GAME_GAMESTATE_HPP
#define CATAN_GAME_GAMESTATE_HPP

#include "Board.hpp"
#include "Game.hpp"

namespace catan_game
{
	class GameState
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		explicit GameState(Context* context);
		~GameState();
		GameState(const GameState&) = delete;
		GameState& operator=(const GameState&) = delete;

		/**************** Getters ****************/
		Board* get_board() const;

	private:
		/**************** Attributes ****************/
		Board* m_board;
		Context* m_context;
		std::vector<ResourceType> m_types_for_hexagons;
		std::vector<int> m_numbers_for_hexagons;

		/**************** Other Methods ****************/
		std::vector<int> generate_numbers_for_hexagons();
		std::vector<ResourceType> generate_types_for_hexagons();
	};

}// namespace catan_game

#endif//CATAN_GAME_GAMESTATE_HPP
