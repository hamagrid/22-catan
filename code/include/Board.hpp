#ifndef CATAN_GAME_BOARD_HPP
#define CATAN_GAME_BOARD_HPP

#include <iostream>
#include <map>
#include <set>
#include <vector>

#include "Bank.hpp"
#include "Hexagon.hpp"
#include "Node.hpp"
#include "Player.hpp"
#include "Road.hpp"

namespace catan_game
{
	class Player;

	class Board
	{
	private:
		/**************** Attributes ****************/
		struct Neighbour
		{
			Node* node;
			Road* road;

			Neighbour(Node* node, Road* road)
				: node(node), road(road)
			{
			}

			Neighbour(const Neighbour&) = delete;
			Neighbour& operator=(const Neighbour&) = delete;
		};

		int m_number_of_nodes;
		int m_current_road_id;
		std::pair<int, int> m_dices;
		std::vector<Road*> m_roads;// use only for deallocation. otherwise access it through the m_adjacency_list
		std::vector<Hexagon*> m_hexagons;
		std::vector<Node*> m_nodes;// use for deallocation and converting node_as_number_label to Node*
		std::vector<std::vector<Neighbour*>> m_adjacency_list;

		/**************** Other Methods ****************/
		void connect_all_neighbour_nodes();
		void construct_hexagons(
			const std::vector<int>& numbers_for_hexagons, const std::vector<ResourceType>& types_for_hexagons
		);

	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		explicit Board(
			const std::vector<int>& numbers_for_hexagons, const std::vector<ResourceType>& types_for_hexagons
		);
		~Board();
		Board(const Board&) = delete;
		Board& operator=(const Board&) = delete;

		/**************** Getters ****************/
		int get_number_of_nodes() const;
		const std::vector<Road*>& get_roads() const;
		const std::vector<Hexagon*>& get_hexagons() const;
		const std::vector<Node*>& get_nodes() const;
		const std::vector<std::vector<Neighbour*>>& get_adjacency_list() const;
		std::set<Node*>& get_offering_nodes();
		Player* get_player_from_owner(Owners& owner, std::vector<Player*>& players);

		const std::pair<int, int> get_dices();

		/**************** Setters **********************/
		void set_roads(sf::Vector2f& mouse, bool is_first_round, Player* player);

		/**************** Other Methods ****************/
		void dfs_print(int start_node_id);
		void dfs_print(Node* current_node);
		Road* get_road_between(int start_node_id, int finish_node_id) const;
		Road* get_road_between(Node* start_node, Node* finish_node) const;
		const std::vector<Neighbour*>& get_neighbours_of_node(int node_id) const;
		const std::vector<Neighbour*>& get_neighbours_of_node(Node* node) const;
		void reset_nodes_for_traversal();
		void reset_roads_for_traversal();
		void update_offering_nodes();
		void reset_nodes_for_setting_houses();
		std::pair<int, int> roll_dices();
		int move(std::vector<Player*>& players, Bank& bank);
		void add_resource_from_second_house(std::vector<Player*>& players, Bank& bank);
		void reset_offerings();
		void check_neighbour(Node* node);
		void dfs(int start_node_id, Player* player);
		void dfs(Node* current_node, int counter, Player* player);
		std::set<Node*> offering_nodes;
		void reset_offering_nodes();
		void reset_robber();
	};

}// namespace catan_game

#endif//CATAN_GAME_BOARD_HPP