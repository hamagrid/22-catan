#ifndef CATAN_GAME_HEXAGON_HPP
#define CATAN_GAME_HEXAGON_HPP

#include <vector>

#include "../include/Node.hpp"
#include "../include/ResourceType.hpp"
#include "../include/Road.hpp"

namespace catan_game
{
	class Hexagon
	{
	private:
		/**************** Attributes ****************/
		int m_id;
		ResourceType m_type;
		int m_number;
		bool m_is_robber_here;
		int m_row;
		int m_column;

		std::vector<Node*> m_vertices;
		Node* m_left_up_node;
		Node* m_up_node;
		Node* m_right_up_node;
		Node* m_right_bottom_node;
		Node* m_bottom_node;
		Node* m_left_bottom_node;

		std::vector<Road*> m_roads;
		Road* m_up_left_road;
		Road* m_up_right_road;
		Road* m_right_road;
		Road* m_down_right_road;
		Road* m_down_left_road;
		Road* m_left_road;
		sf::Vector2f m_center;
		/**************** Other Methods ****************/
		std::pair<int, int> calculate_row_and_column() const;

	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		explicit Hexagon(
			int id
			, Node* left_up_node
			, Node* up_node
			, Node* right_up_node
			, Node* right_bottom_node
			, Node* bottom_node
			, Node* left_bottom_node
			, Road* up_left_road
			, Road* up_right_road
			, Road* right_road
			, Road* down_right_road
			, Road* down_left_road
			, Road* left_road
			, ResourceType type
			, int number
		);
		Hexagon(const Hexagon&) = delete;
		Hexagon& operator=(const Hexagon&) = delete;

		/**************** Getters ****************/
		int get_id() const;
		ResourceType get_type() const;
		int get_number() const;
		bool is_robber_here() const;
		int get_row() const;
		int get_column() const;
		const std::vector<Node*>& get_nodes() const;
		Node* get_left_up_node() const;
		Node* get_up_node() const;
		Node* get_right_up_node() const;
		Node* get_right_bottom_node() const;
		Node* get_bottom_node() const;
		Node* get_left_bottom_node() const;
		const std::vector<Road*>& get_roads() const;
		Road* get_up_left_road() const;
		Road* get_up_right_road() const;
		Road* get_right_road() const;
		Road* get_down_right_road() const;
		Road* get_down_left_road() const;
		Road* get_left_road() const;
		sf::Vector2f get_center();
		bool is_clicked(sf::Vector2f);
		/**************** Setters ****************/
		void set_is_robber_here(bool value);
		void set_center(sf::Vector2f);
	};

}// namespace catan_game

#endif//CATAN_GAME_HEXAGON_HPP