#ifndef CATAN_GAME_ResourceType_HPP
#define CATAN_GAME_ResourceType_HPP

namespace catan_game
{
	enum class ResourceType
	{
		Wood, // drvo
		Brick,// cigla
		Wool, // vuna
		Wheat,// psenica
		Stone,// kamen
		Desert// pustinja
	};

}

#endif//CATAN_GAME_ResourceType_HPP