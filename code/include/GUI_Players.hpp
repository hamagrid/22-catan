#ifndef CATAN_GAME_GUI_PLAYERS_HPP
#define CATAN_GAME_GUI_PLAYERS_HPP

#include <vector>

#include "SFML/Graphics.hpp"

#include "Game.hpp"
#include "Player.hpp"

namespace catan_game
{
	class GUI_Players
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GUI_Players(Context* context);
		~GUI_Players();
		GUI_Players(const GUI_Players&) = delete;
		GUI_Players& operator=(const GUI_Players&) = delete;

		/**************** Other Methods ****************/
		void react_to_click(sf::Event& e);
		const std::map<int, std::map<ResourceType, int>>& GetMTradeValues() const;
		void clear_trade_value();
		int GetMPlayerToTradeWith() const;
		void draw_players();
	private:
		/**************** Attributes ****************/
		Context* m_context;
		sf::Font m_font;
		std::map<int, std::map<ResourceType, int>> m_trade_values;
		int m_player_to_trade_with;

		struct m_player_field
		{
			sf::Text m_name;
			sf::RectangleShape m_sh;
			sf::Texture m_texture_brick;
			sf::Texture m_texture_stone;
			sf::Texture m_texture_wheat;
			sf::Texture m_texture_wood;
			sf::Texture m_texture_wool;
			sf::Sprite m_sprite_brick;
			sf::Sprite m_sprite_stone;
			sf::Sprite m_sprite_wheat;
			sf::Sprite m_sprite_wood;
			sf::Sprite m_sprite_wool;
			sf::Text m_text_brick;
			sf::Text m_text_stone;
			sf::Text m_text_wheat;
			sf::Text m_text_wood;
			sf::Text m_text_wool;
			sf::Text m_text_victory_points;
			sf::Text m_text_longest_road;
		};
		std::vector<m_player_field> m_fields;
		const int CHAR_SIZE = 20;

		int Y_COORDS[5] = { 0, 20, 105, 190, 275 };
		const int X_ID = 700;
		/**************** Other Methods ****************/
		void init_fields();
		void init_field(int id);

		void update_field(int id);
	};
}// namespace catan_game

#endif//CATAN_GAME_GUI_PLAYERS_HPP
