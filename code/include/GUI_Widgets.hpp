#ifndef CATAN_GAME_GUI_WIDGETS_HPP
#define CATAN_GAME_GUI_WIDGETS_HPP

#include <vector>

#include "SFML/Graphics.hpp"

#include "Game.hpp"
#include "GameState.hpp"
#include "Player.hpp"

namespace catan_game
{
	class GUI_Widgets
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GUI_Widgets(Context* context, GameState* game_state);
		~GUI_Widgets();
		GUI_Widgets(const GUI_Widgets&) = delete;
		GUI_Widgets& operator=(const GUI_Widgets&) = delete;

		/**************** Other Methods ****************/
		void draw(const std::basic_string<char> player_name, sf::Color color);
		void react_to_click(sf::Event& e);

		bool is_plain_road_clicked(sf::Vector2f&) const;
		bool is_plain_house_clicked(sf::Vector2f&) const;
		bool is_plain_city_clicked(sf::Vector2f&) const;

		bool is_dices_clicked(sf::Vector2f&) const;
		bool is_end_move_clicked(sf::Vector2f&) const;

	private:
		/**************** Attributes ****************/
		Context* m_context;
		GameState* m_game_state;
		sf::Font m_font;
		sf::Texture m_texture_road;
		sf::Sprite m_plain_road;
		sf::Texture m_texture_house;
		sf::Sprite m_plain_house;
		sf::Texture m_texture_city;
		sf::Sprite m_plain_city;

		sf::Texture m_texture_one;
		sf::Texture m_texture_two;
		sf::Texture m_texture_three;
		sf::Texture m_texture_four;
		sf::Texture m_texture_five;
		sf::Texture m_texture_six;

		sf::Sprite m_first_dice;
		sf::Sprite m_second_dice;

		sf::Text m_turn;

		sf::Text m_end_move;
		sf::Text m_dices;

		void initialise_shop();
		void initialise_dices();
		void update_on_turn(
			const std::basic_string<char> player_name, sf::Color color
		);
		void set_dices();
	};
}// namespace catan_game

#endif// CATAN_GAME_GUI_WIDGETS_HPP
