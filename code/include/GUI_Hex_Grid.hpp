#ifndef CATAN_GAME_GUI_HEX_GRID_HPP
#define CATAN_GAME_GUI_HEX_GRID_HPP

#include <vector>

#include "GUI_City.hpp"
#include "GUI_Hex.hpp"
#include "GUI_House.hpp"
#include "GUI_Robber.hpp"
#include "GameState.hpp"
#include "GUI_Is_Offering_Button.hpp"

namespace catan_game
{
	class GUI_Hex_Grid
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GUI_Hex_Grid(
			GameState* game_state, Context* context, float first_hex_x, float first_hex_y, float first_hex_side_length
		);
		~GUI_Hex_Grid();
		GUI_Hex_Grid(const GUI_Hex_Grid&) = delete;
		GUI_Hex_Grid& operator=(const GUI_Hex_Grid&) = delete;

		/**************** Other Methods ****************/
		float column_offset_fix_y(int row);
		float row_offset_fix_x(int row);
		void draw();

	private:
		/**************** Attributes ****************/
		std::vector<GUI_Hex*> m_gui_hexagons;
		GameState* m_game_state;
		Context* m_context;
		float m_first_hex_side_length;
		std::map<Owners, GUI_House*> m_gui_houses;
		std::map<Owners, GUI_City*> m_gui_cities;
		GUI_Robber* m_gui_robber;
		GUI_Is_Offering_Button* m_is_offering_button;

		void set_nodes_coordinates();
		void set_roads_coordinates();
	};

}// namespace catan_game

#endif//CATAN_GAME_GUI_HEX_GRID_HPP