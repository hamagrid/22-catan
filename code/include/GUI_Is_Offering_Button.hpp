#ifndef CATAN_GAME_CODE_INCLUDE_GUI_IS_OFFERING_BUTTON_HPP
#define CATAN_GAME_CODE_INCLUDE_GUI_IS_OFFERING_BUTTON_HPP

#include "GUI_Sprite_Object.hpp"

namespace catan_game
{
	class GUI_Is_Offering_Button : public GUI_Sprite_Object
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GUI_Is_Offering_Button(float center_x, float center_y, Texture_ID texture_id, Context* context);
		GUI_Is_Offering_Button(const GUI_Is_Offering_Button&) = delete;
		GUI_Is_Offering_Button& operator=(const GUI_Is_Offering_Button&) = delete;
	};

}// namespace catan_game

#endif //CATAN_GAME_CODE_INCLUDE_GUI_IS_OFFERING_BUTTON_HPP
