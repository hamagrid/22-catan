#ifndef CATAN_GAME_GUI_HEX_HPP
#define CATAN_GAME_GUI_HEX_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System.hpp>

#include "GUI_Number_Box.hpp"
#include "GUI_Sprite_Object.hpp"
#include "Game.hpp"
#include "Hexagon.hpp"
#include "ResourceType.hpp"

namespace catan_game
{
	class GUI_Hex : public GUI_Sprite_Object
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		GUI_Hex(
			float center_x
			, float center_y
			, ResourceType type
			, int number
			, float side_length
			, Context* context
			, Hexagon* hexagon
		);
		~GUI_Hex();
		GUI_Hex(const GUI_Hex&) = delete;
		GUI_Hex& operator=(const GUI_Hex&) = delete;

		/**************** Getters ****************/
		Hexagon* get_hexagon() const;

		std::vector<sf::Vector2f> get_nodes_coordinats();

		/**************** Other Methods ****************/
		void draw() override;

	private:
		/**************** Attributes ****************/
		float m_side_length;
		float m_triangle_height;
		int m_number;
		ResourceType m_type;
		GUI_Number_Box* m_number_box;
		Hexagon* m_hexagon;

		/**************** Other Methods ****************/
		Texture_ID resource_type_to_texture_id(ResourceType rt);
	};
}// namespace catan_game

#endif//CATAN_GAME_GUI_HEX_HPP