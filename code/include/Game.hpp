#ifndef CATAN_GAME_GAME_HPP
#define CATAN_GAME_GAME_HPP

#include <SFML/Graphics/RenderWindow.hpp>

#include "AssetManager.hpp"
#include "PageManager.hpp"

namespace catan_game
{
	struct Context
	{
		AssetManager* m_assets;
		PageManager* m_states;
		sf::RenderWindow* m_window;

		Context()
		{
			m_assets = new AssetManager();
			m_states = new PageManager();
			m_window = new sf::RenderWindow();
		}

		~Context()
		{
			delete m_assets;
			delete m_states;
			delete m_window;
		}

		Context(const Context&) = delete;
		Context& operator=(const Context&) = delete;
	};

	class Game
	{
	public:
		/**************** Constructors/Destructor/AssignmentOperator ****************/
		Game();
		~Game();
		Game(const Game&) = delete;
		Game& operator=(const Game&) = delete;

		/**************** Other Methods ****************/
		void run();

	private:
		/**************** Attributes ****************/
		Context* m_context;
		const sf::Time TIME_PER_FRAME = sf::seconds(1.f / 60.f);
		const int WIDTH = 1000;
		const int HEIGHT = 700;
	};

}// namespace catan_game

#endif//CATAN_GAME_GAME_HPP
